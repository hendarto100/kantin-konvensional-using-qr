<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyKeranjangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        Schema::table('keranjang', function (Blueprint $table) {
          $table->foreign('penjual_konven_id')->references('id')->on('penjual_konven');
          $table->foreign('barang_konven_id')->references('id')->on('barang_konven');
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
