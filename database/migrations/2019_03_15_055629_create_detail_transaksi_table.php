<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailTransaksiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail_transaksi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('stok_barang_id')->unsigned()->index('detail_transaksi_stok_barang_id_foreign');
			$table->integer('transaksi_id')->unsigned()->index('detail_transaksi_transaksi_id_foreign');
			$table->integer('kuantitas')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_transaksi');
	}

}
