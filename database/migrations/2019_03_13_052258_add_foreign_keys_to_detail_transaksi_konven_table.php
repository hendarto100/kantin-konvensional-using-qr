<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetailTransaksiKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detail_transaksi_konven', function(Blueprint $table)
		{
			$table->foreign('barang_konven_id')->references('id')->on('barang_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('transaksi_konven_id')->references('id')->on('transaksi_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detail_transaksi_konven', function(Blueprint $table)
		{
			$table->dropForeign('detail_transaksi_konven_barang_konven_id_foreign');
			$table->dropForeign('detail_transaksi_konven_transaksi_konven_id_foreign');
		});
	}

}
