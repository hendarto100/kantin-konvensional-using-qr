<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangJualTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barang_jual', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjual_id')->unsigned()->index('barang_jual_penjual_id_foreign');
			$table->integer('barang_id')->unsigned()->index('barang_jual_barang_id_foreign');
			$table->integer('harga');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barang_jual');
	}

}
