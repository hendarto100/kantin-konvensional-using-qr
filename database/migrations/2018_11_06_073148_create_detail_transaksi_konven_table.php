<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransaksiKonvenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_konven', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barang_konven_id')->unsigned();
            $table->integer('transaksi_konven_id')->unsigned();
            $table->integer('harga')->unsigned();
            $table->integer('jumlah')->unsigned();
            $table->integer('diskon')->unsigned();
            $table->integer('total')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_konven');
    }
}
