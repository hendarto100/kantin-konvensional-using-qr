<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyDetailTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('detail_transaksi', function (Blueprint $table) {
          // $table->increments('id');
          // $table->integer('stok_barang_id');
          // $table->integer('transaksi_id');
          // $table->timestamps();
          $table->foreign('stok_barang_id')->references('id')->on('stok_barang');
          $table->foreign('transaksi_id')->references('id')->on('transaksi');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
