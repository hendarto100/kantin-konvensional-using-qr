<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailTransaksiKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail_transaksi_konven', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('barang_konven_id')->unsigned()->index('detail_transaksi_konven_barang_konven_id_foreign');
			$table->integer('transaksi_konven_id')->unsigned()->index('detail_transaksi_konven_transaksi_konven_id_foreign');
			$table->integer('harga')->unsigned();
			$table->integer('jumlah')->unsigned();
			$table->integer('diskon')->unsigned();
			$table->integer('total')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_transaksi_konven');
	}

}
