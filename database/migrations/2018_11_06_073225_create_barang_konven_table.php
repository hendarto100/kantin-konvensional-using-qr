<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangKonvenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_konven', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penjual_id')->unsigned();
            $table->integer('kategori_id')->unsigned();
            $table->string('nama');
            $table->string('deskripsi');
            $table->integer('harga')->unsigned();
            $table->integer('stok')->unsigned();
            $table->integer('diskon')->unsigned();
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_konven');
    }
}
