<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barang_konven', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjual_konven_id')->unsigned()->index('penjual_konven_id');
			$table->integer('kategori_id')->unsigned()->index('barang_konven_kategori_id_foreign');
			$table->string('nama', 191);
			$table->string('deskripsi', 191);
			$table->integer('harga')->unsigned();
			$table->integer('stok')->unsigned();
			$table->integer('diskon')->unsigned()->nullable();
			$table->string('foto', 191)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barang_konven');
	}

}
