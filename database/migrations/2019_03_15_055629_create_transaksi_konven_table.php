<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksiKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_konven', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjual_konven_id')->unsigned()->index('penjual_konven_id');
			$table->integer('pembeli_id')->unsigned()->index('transaksi_konven_pembeli_id_foreign');
			$table->integer('jenis_pembayaran_id')->unsigned()->index('transaksi_konven_jenis_pembayaran_id_foreign');
			$table->string('status', 191);
			$table->integer('diskon')->nullable();
			$table->integer('total')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi_konven');
	}

}
