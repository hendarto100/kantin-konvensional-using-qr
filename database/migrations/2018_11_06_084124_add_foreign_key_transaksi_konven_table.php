<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTransaksiKonvenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('transaksi_konven', function (Blueprint $table) {
        $table->foreign('pembeli_id')->references('id')->on('pembeli');
        $table->foreign('jenis_pembayaran_id')->references('id')->on('jenis_pembayaran');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
