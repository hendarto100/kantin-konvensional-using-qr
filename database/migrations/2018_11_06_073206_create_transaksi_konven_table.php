<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiKonvenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_konven', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pembeli_id')->unsigned();
            $table->integer('jenis_pembayaran_id')->unsigned();
            $table->string('status');
            $table->integer('total')->unsigned();
            $table->integer('pajak')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_konven');
    }
}
