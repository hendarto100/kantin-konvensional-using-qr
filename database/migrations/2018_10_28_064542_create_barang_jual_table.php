<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangJualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_jual', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('penjual_id')->unsigned();
          $table->integer('barang_id')->unsigned();
          $table->integer('harga');
          $table->timestamps();
          // $table->foreign('user_id')->references('id')->on('users');
          // $table->foreign('barang_id')->references('id')->on('barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_jual');
    }
}
