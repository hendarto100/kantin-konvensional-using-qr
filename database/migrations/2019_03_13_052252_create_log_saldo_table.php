<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogSaldoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_saldo', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index('log_saldo_user_id_foreign');
			$table->integer('reference_id')->unsigned();
			$table->string('source', 191);
			$table->integer('saldo_awal')->unsigned();
			$table->enum('type', array('topup','withdraw','purchase','sales'));
			$table->integer('total')->unsigned()->default(0);
			$table->integer('grand_total')->unsigned()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_saldo');
	}

}
