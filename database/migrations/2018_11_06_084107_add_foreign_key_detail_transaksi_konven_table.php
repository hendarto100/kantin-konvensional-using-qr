<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyDetailTransaksiKonvenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('detail_transaksi_konven', function (Blueprint $table) {
        $table->foreign('barang_konven_id')->references('id')->on('barang_konven');
        $table->foreign('transaksi_konven_id')->references('id')->on('transaksi_konven');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
