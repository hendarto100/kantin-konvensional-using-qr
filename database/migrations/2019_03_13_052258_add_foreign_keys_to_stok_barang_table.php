<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStokBarangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stok_barang', function(Blueprint $table)
		{
			$table->foreign('barang_jual_id')->references('id')->on('barang_jual')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('status_harian_id')->references('id')->on('status_harian')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('stok_barang', function(Blueprint $table)
		{
			$table->dropForeign('stok_barang_barang_jual_id_foreign');
			$table->dropForeign('stok_barang_status_harian_id_foreign');
		});
	}

}
