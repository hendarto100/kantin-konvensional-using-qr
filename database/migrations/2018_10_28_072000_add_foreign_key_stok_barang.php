<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyStokBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('stok_barang', function (Blueprint $table) {
          // $table->increments('id');
          // $table->integer('barang_jual_id');
          // $table->integer('status_harian_id');
          // $table->integer('jumlah_id');
          // $table->timestamps();
          $table->foreign('barang_jual_id')->references('id')->on('barang_jual');
          $table->foreign('status_harian_id')->references('id')->on('status_harian');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
