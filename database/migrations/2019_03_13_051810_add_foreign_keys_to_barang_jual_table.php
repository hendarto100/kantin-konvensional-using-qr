<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBarangJualTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('barang_jual', function(Blueprint $table)
		{
			$table->foreign('barang_id')->references('id')->on('barang')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('penjual_id')->references('id')->on('penjual')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('barang_jual', function(Blueprint $table)
		{
			$table->dropForeign('barang_jual_barang_id_foreign');
			$table->dropForeign('barang_jual_penjual_id_foreign');
		});
	}

}
