<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetailTransaksiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detail_transaksi', function(Blueprint $table)
		{
			$table->foreign('stok_barang_id')->references('id')->on('stok_barang')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('transaksi_id')->references('id')->on('transaksi')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detail_transaksi', function(Blueprint $table)
		{
			$table->dropForeign('detail_transaksi_stok_barang_id_foreign');
			$table->dropForeign('detail_transaksi_transaksi_id_foreign');
		});
	}

}
