<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToKeranjangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('keranjang', function(Blueprint $table)
		{
			$table->foreign('barang_konven_id')->references('id')->on('barang_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('penjual_konven_id')->references('id')->on('penjual_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('keranjang', function(Blueprint $table)
		{
			$table->dropForeign('keranjang_barang_konven_id_foreign');
			$table->dropForeign('keranjang_penjual_konven_id_foreign');
		});
	}

}
