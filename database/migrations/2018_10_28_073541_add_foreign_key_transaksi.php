<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('transaksi', function (Blueprint $table) {
          // $table->increments('id');
          // $table->integer('user_id');
          // $table->integer('jenis_pembayaran_id');
          // $table->timestamps();
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('jenis_pembayaran_id')->references('id')->on('jenis_pembayaran');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
