<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barang_jual_id')->unsigned();
            $table->integer('status_harian_id')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();
            // $table->foreign('barang_jual_id')->references('id')->on('barang_jual');
            // $table->foreign('status_harian_id')->references('id')->on('status_harian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok_barang');
    }
}
