<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('barang', function (Blueprint $table) {
          // $table->increments('id');
          // $table->integer('kategori_id');
          // $table->string('nama');
          // $table->timestamps();
          $table->foreign('kategori_id')->references('id')->on('kategori');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
