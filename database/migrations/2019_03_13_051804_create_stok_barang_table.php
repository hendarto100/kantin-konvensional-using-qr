<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStokBarangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stok_barang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('barang_jual_id')->unsigned()->index('stok_barang_barang_jual_id_foreign');
			$table->integer('status_harian_id')->unsigned()->index('stok_barang_status_harian_id_foreign');
			$table->integer('jumlah');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stok_barang');
	}

}
