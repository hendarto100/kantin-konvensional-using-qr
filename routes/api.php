<?php

use Illuminate\Http\Request;
use app\Providers\RouteServiceProvider;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API FOR ANDROID
Route::middleware('auth:api')->get('logout', 'UserController@api_logout');
// PEMBELI
Route::group(['prefix' => 'pembeli'], function()
{
  Route::middleware('api')->post('/register', 'UserController@api_registerPembeli');
  Route::middleware('api')->post('/login','UserController@api_loginPembeli');
  Route::middleware('auth:api')->put('/edit','UserController@api_updatePembeli');
  Route::middleware('auth:api')->put('/upload-ava','UserController@api_uploadAva');
  Route::middleware('auth:api')->put('/edit-password','UserController@api_updatePassword');
  Route::middleware('auth:api')->get('/detail', 'UserController@api_detail');
  Route::middleware('auth:api')->get('/saldo/riwayat', 'LogSaldoController@riwayat');
  Route::middleware('auth:api')->get('/jenis-pembayaran', 'JenisPembayaranController@api_index');
  Route::middleware('auth:api')->put('/transaksi/pay/{id}', 'TransaksiKonvenController@api_pay');
});

// PENJUAL
Route::group(['prefix' => 'penjual'], function()
{
  Route::middleware('api')->post('/login','UserController@api_loginPenjual');
  Route::middleware('auth:api')->get('/barang_konven_penjual', 'BarangKonvenController@api_index_penjual');
  Route::middleware('auth:api')->get('/jenis_pembayaran', 'JenisPembayaranController@api_index');
  Route::middleware('auth:api')->get('/keranjang', 'KeranjangController@api_index');
  Route::middleware('auth:api')->post('/keranjang/create/{id}', 'KeranjangController@api_store');
  Route::middleware('auth:api')->put('/keranjang/edit/{id}', 'KeranjangController@api_update');
  Route::middleware('auth:api')->delete('/keranjang/delete/{id}', 'KeranjangController@api_destroy');
  Route::middleware('auth:api')->delete('/keranjang/delete/', 'KeranjangController@api_destroyAll');
  Route::middleware('auth:api')->post('/transaksi/create', 'TransaksiKonvenController@api_store');
});

// ================================BARANG================================
Route::middleware('auth:api')->get('/barang_konven', 'BarangKonvenController@api_index');
Route::middleware('auth:api')->post('/barang_konven/create', 'BarangController@api_store');
Route::middleware('auth:api')->put('/barang/update/{id}', 'BarangController@api_update');
Route::middleware('auth:api')->get('/barang_konven/{id}', 'BarangController@api_detail');
Route::middleware('auth:api')->delete('/barang/delete/{id}', 'BarangController@api_destroy');

// ================================BARANG================================PENJUAL===

//tak tambahi

Route::middleware('auth:api')->get('/penjual/barang_konven', 'PenjualKonvenController@api_index_barang');
Route::middleware('auth:api')->post('/penjual/transaksi_konven', 'PenjualKonvenController@api_transaksi');
Route::middleware('auth:api')->post('/penjual/cek_pin', 'PenjualKonvenController@api_cek_pin');
