<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'DashboardController@index');

Auth::routes();
Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/transaksi_admin', 'TransaksiController@index')->middleware('isAdmin');
Route::post('/transaksi_admin', 'TransaksiController@index')->middleware('isAdmin');
Route::get('/transaksi_admin/find', 'TransaksiController@find')->middleware('isAdmin');
Route::get('/transaksi_admin/{id}', 'TransaksiController@detail')->middleware('isAdmin');

// ====================== PROFILE ADMIN =========================
Route::get('/profile/{id}', 'ProfilController@index')->middleware('isAdmin');
Route::get('/profile/{id}/edit', 'ProfilController@edit')->middleware('isAdmin');
Route::post('/profile/{id}/edit', 'ProfilController@update')->middleware('isAdmin');
Route::get('/profile/{id}/edit-password', 'ProfilController@editPassword')->middleware('isAdmin');
Route::post('/profile/{id}/edit-password', 'ProfilController@updatePassword')->middleware('isAdmin');
Route::get('/profile/{id}/edit-avatar', 'ProfilController@editAvatar')->middleware('isAdmin');
Route::post('/profile/{id}/edit-avatar', 'ProfilController@updateAvatar')->middleware('isAdmin');

// ====================== PROFILE KASIR =========================
Route::get('/profil_pos/{id}', 'ProfilPosController@index')->middleware('isKasir');
Route::get('/profil_pos/{id}/edit', 'ProfilPosController@edit')->middleware('isKasir');
Route::post('/profil_pos/{id}/edit', 'ProfilPosController@update')->middleware('isKasir');
Route::get('/profil_pos/{id}/edit-password', 'ProfilPosController@editPassword')->middleware('isKasir');
Route::post('/profil_pos/{id}/edit-password', 'ProfilPosController@updatePassword')->middleware('isKasir');
Route::get('/profil_pos/{id}/edit-avatar', 'ProfilPosController@editAvatar')->middleware('isKasir');
Route::post('/profil_pos/{id}/edit-avatar', 'ProfilPosController@updateAvatar')->middleware('isKasir');
Route::post('/profil_pos/{id}/mutasi-saldo', 'ProfilPosController@mutasiSaldo')->middleware('isKasir');

// ====================== START OF PENJUAL =========================
  Route::group(['prefix' => 'master/penjual_konven'], function()
  {
    Route::get('/', 'PenjualKonvenController@index');
    Route::get('/aktif', 'PenjualKonvenController@aktif');
    Route::get('/non-aktif', 'PenjualKonvenController@nonAktif');
    Route::get('/add', 'PenjualKonvenController@create');
    Route::post('/store', 'PenjualKonvenController@store');
    Route::get('/edit/{id}', 'PenjualKonvenController@edit');
    Route::post('/edit/{id}', 'PenjualKonvenController@update');
    Route::get('/edit/{id}/password', 'PenjualKonvenController@editPassword');
    Route::post('/edit/{id}/password', 'PenjualKonvenController@updatePassword');
    Route::get('/edit/{id}/avatar', 'PenjualKonvenController@editAvatar');
    Route::post('/edit/{id}/avatar', 'PenjualKonvenController@updateAvatar');
    Route::get('/{id}','PenjualKonvenController@detail');
    Route::get('/status/{id}', 'PenjualKonvenController@status');
    Route::get('/barang-jual/{id}','BarangJualController@index');
    Route::get('/barang-jual/{id}/download','BarangJualController@downloadQRCode');
    Route::get('/barang-jual/download/{id}','BarangJualController@downloadAllQRCode');
  });

// ====================== START OF PEMBELI =========================

    Route::get('master/pembeli', 'PembeliMasterController@index')->middleware('isAdmin');
    Route::get('master/pembeli_bukan_member', 'PembeliMasterController@index2')->middleware('isAdmin');
    Route::get('master/pembeli/aktif', 'PembeliMasterController@aktif')->middleware('isAdmin');
    Route::get('master/pembeli/non-aktif', 'PembeliMasterController@nonAktif')->middleware('isAdmin');
    Route::get('master/pembeli/add', 'PembeliMasterController@create')->middleware('isAdmin');
    Route::get('master/pembeli/add/nonmember/{id}', 'PembeliMasterController@create2')->middleware('isAdmin');
    Route::post('master/pembeli/store', 'PembeliMasterController@store')->middleware('isAdmin');
    Route::post('master/pembeli/store2/{id}', 'PembeliMasterController@store2')->middleware('isAdmin');
    Route::get('master/pembeli/edit/{id}', 'PembeliMasterController@edit')->middleware('isAdmin');
    Route::post('master/pembeli/edit/{id}', 'PembeliMasterController@update')->middleware('isAdmin');
    Route::get('master/pembeli/edit/{id}/password', 'PembeliMasterController@editPassword')->middleware('isAdmin');
    Route::post('master/pembeli/edit/{id}/password', 'PembeliMasterController@updatePassword')->middleware('isAdmin');
    Route::get('master/pembeli/edit/{id}/tambah-saldo', 'TopupController@create')->middleware('isAdmin');
    Route::get('master/pembeli/edit/{id}/avatar', 'PembeliMasterController@editAvatar')->middleware('isAdmin');
    Route::post('master/pembeli/edit/{id}/avatar', 'PembeliMasterController@updateAvatar')->middleware('isAdmin');
    Route::get('master/pembeli/status/{id}', 'PembeliMasterController@status')->middleware('isAdmin');
    Route::get('master/pembeli/{id}','PembeliMasterController@detail')->middleware('isAdmin');

// ====================== START OF LAPORAN =========================

Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'laporan'], function()
{
  // START OF lAPORAN PENJUAL
  Route::get('/penjual', 'PenjualReportController@index');
  Route::post('/penjual', 'PenjualReportController@index');
  Route::get('/penjual/find', 'PenjualReportController@laporan');
  Route::get('/penjual/downloadPDF/','PenjualReportController@downloadPDF');
  Route::get('/penjual/downloadExcel', 'PenjualReportController@downloadExcel');
  Route::get('/penjual/stok-barang/{id}', 'PenjualReportController@detailStok');
  // START OF lAPORAN PEMBELI
  Route::get('/pembeli', 'PembeliReportController@index');
  Route::post('/pembeli', 'PembeliReportController@index');
  Route::get('/pembeli/find', 'PembeliReportController@laporan');
  Route::get('/pembeli/downloadPDF/','PembeliReportController@downloadPDF');
  Route::get('/pembeli/downloadExcel', 'PembeliReportController@downloadExcel');
  // START OF lAPORAN BARANG
  Route::get('/barang', 'BarangReportController@index');
  Route::post('/barang', 'BarangReportController@index');
  Route::get('/barang/find', 'BarangReportController@laporan');
  Route::get('/barang/downloadPDF/','BarangReportController@downloadPDF');
  Route::get('/barang/downloadExcel', 'BarangReportController@downloadExcel');
});

// ====================== TOP UP =========================
      Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'topup'], function()
      {
        Route::get('/', 'TopupController@index');
        Route::get('/detail-user/{id}', 'TopupController@detail');
        Route::post('/{id}/tambah-saldo', 'TopupController@store');
      });

Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'pencairan'], function()
{
  // Route::get('/', 'PencairanController@index');
  Route::get('/', 'PencairanController@find');
  Route::get('/find', 'PencairanController@index');
  Route::get('/detail-user/{id}', 'PencairanController@detail');
  Route::post('/{id}/tambah-saldo', 'PencairanController@store');
  Route::get('/{id}', 'PencairanController@detail');
  Route::post('/{id}', 'PencairanController@update');
});


// ====================== START OF MASTER DATA =========================

      // START OF MASTER BARANG
      Route::get('master/barang', 'BarangController@index')->middleware('isAdmin');
      Route::post('master/barang', 'BarangController@index')->middleware('isAdmin');
      Route::get('master/barang/add', 'BarangController@create')->middleware('isAdmin');
      Route::post('master/barang/store', 'BarangController@store')->middleware('isAdmin');
      Route::get('master/barang/edit/{id}', 'BarangController@edit')->middleware('isAdmin');
      Route::post('master/barang/edit/{id}', 'BarangController@update')->middleware('isAdmin');
      Route::get('master/barang/delete/{id}', 'BarangController@destroy')->middleware('isAdmin');


      // START OF MASTER BARANG
      Route::get('master/jenis-pembayaran', 'JenisPembayaranController@index')->middleware('isAdmin');
      Route::post('master/jenis-pembayaran', 'JenisPembayaranController@index')->middleware('isAdmin');
      Route::get('master/jenis-pembayaran/add', 'JenisPembayaranController@create')->middleware('isAdmin');
      Route::post('master/jenis-pembayaran/store', 'JenisPembayaranController@store')->middleware('isAdmin');
      Route::get('master/jenis-pembayaran/edit/{id}', 'JenisPembayaranController@edit')->middleware('isAdmin');
      Route::post('master/jenis-pembayaran/edit/{id}', 'JenisPembayaranController@update')->middleware('isAdmin');
      Route::get('master/jenis-pembayaran/delete/{id}', 'JenisPembayaranController@destroy')->middleware('isAdmin');


      // START OF MASTER KATEGORI BARANG
      Route::get('master/kategori', 'KategoriController@index')->middleware('isAdmin');
      Route::post('master/kategori', 'KategoriController@index')->middleware('isAdmin');
      Route::get('master/kategori/add', 'KategoriController@create')->middleware('isAdmin');
      Route::post('master/kategori/store', 'KategoriController@store')->middleware('isAdmin');
      Route::get('master/kategori/edit/{id}', 'KategoriController@edit')->middleware('isAdmin');
      Route::post('master/kategori/edit/{id}', 'KategoriController@update')->middleware('isAdmin');
      Route::get('master/kategori/delete/{id}', 'KategoriController@destroy')->middleware('isAdmin');

      // START OF MASTER BANK
      Route::group(['prefix' => 'master/bank'], function()
      {
        Route::get('/', 'BankController@index');
        Route::post('/', 'BankController@index');
        Route::get('/add', 'BankController@create');
        Route::post('/store', 'BankController@store');
        Route::get('/edit/{id}', 'BankController@edit');
        Route::post('/edit/{id}', 'BankController@update');
        Route::get('/deleted', 'BankController@withTrashed');
        Route::get('/restore/{id}', 'BankController@restore');
        Route::get('/force-delete/{id}', 'BankController@forceDelete');
      });

      // START OF STATUS
      Route::get('master/status', 'statusController@index')->middleware('isAdmin');
      Route::post('master/status', 'statusController@index')->middleware('isAdmin');
      Route::get('master/status/add', 'statusController@create')->middleware('isAdmin');
      Route::post('master/status/store', 'statusController@store')->middleware('isAdmin');
      Route::get('master/status/edit/{id}', 'statusController@edit')->middleware('isAdmin');
      Route::post('master/status/edit/{id}', 'statusController@update')->middleware('isAdmin');
      Route::get('master/status/delete/{id}', 'statusController@destroy')->middleware('isAdmin');


      // START OF STATUS STOK HARIAN
      Route::get('master/status-harian', 'StatusHarianController@index')->middleware('isAdmin');
      Route::post('master/status-harian', 'StatusHarianController@index')->middleware('isAdmin');
      Route::get('master/status-harian/add', 'StatusHarianController@create')->middleware('isAdmin');
      Route::post('master/status-harian/store', 'StatusHarianController@store')->middleware('isAdmin');
      Route::get('master/status-harian/edit/{id}', 'StatusHarianController@edit')->middleware('isAdmin');
      Route::post('master/status-harian/edit/{id}', 'StatusHarianController@update')->middleware('isAdmin');
      Route::get('master/status-harian/delete/{id}', 'StatusHarianController@destroy')->middleware('isAdmin');


      

      Route::group(['prefix' => 'master/barang_konven'], function()
      {
        Route::get('/', 'BarangKonvenController@index');
        Route::get('/add', 'BarangKonvenController@create');
        Route::post('/store', 'BarangKonvenController@store');
        Route::get('/edit/{id}', 'BarangKonvenController@edit');
        Route::post('/edit/{id}', 'BarangKonvenController@update');
        Route::get('/delete/{id}', 'BarangKonvenController@delete');
        Route::get('/restore/{id}', 'BarangKonvenController@restore');
        Route::get('/force-delete/{id}', 'BarangKonvenController@forceDelete');
      });


      Route::get('dashboard_konven', 'DashboardKonvenController@index')->middleware('isKasir');
      Route::get('transaksi_konven', 'DetailTransaksiKonvenController@index')->middleware('isKasir');
      Route::get('transaksi_konven/search','DetailTransaksiKonvenController@search')->middleware('isKasir');
      Route::get('transaksi_konven/list','TransaksiKonvenController@index')->middleware('isKasir');
      Route::get('transaksi_konven/detail_transaksi_konven/{id}','TransaksiKonvenController@showDetail')->middleware('isKasir');
      Route::get('transaksi_konven/export', 'TransaksiKonvenController@exportFile')->middleware('isKasir');
      Route::resource('transaksi_konven', 'DetailTransaksiKonvenController')->middleware('isKasir');
      Route::get('barang_konven/list_pos', 'BarangKonvenController@index2')->middleware('isKasir');
      Route::get('barang_konven/tambah_barang_pos', 'BarangKonvenController@create2')->middleware('isKasir');
      Route::get('barang_konven/edit_pos/{id}', 'BarangKonvenController@edit2')->middleware('isKasir');
      Route::post('barang_konven/edit_pos/{id}', 'BarangKonvenController@update2')->middleware('isKasir');
      Route::post('barang_konven/store_pos', 'BarangKonvenController@store2')->middleware('isKasir');
      Route::get('barang_konven/delete/{id}', 'BarangKonvenController@destroy')->middleware('isKasir');
      Route::get('barang_konven/export', 'BarangKonvenController@exportFile')->middleware('isKasir');
      Route::get('pembeli_konven/list_pos', 'PembeliController@index2')->middleware('isKasir');
      Route::get ('addItem', 'DetailTransaksiKonvenController@tampilKeranjang')->middleware('isKasir');
      Route::post ('addItem', 'DetailTransaksiKonvenController@addItem')->middleware('isKasir');
      Route::post ('tambahTransaksi', 'DetailTransaksiKonvenController@tambahTransaksi')->middleware('isKasir');
      Route::get('transaksi_konven/{id_produk}/getData', 'DetailTransaksiKonvenController@getData')->middleware('isKasir');
      Route::get('transaksi_konven/{nama}/getDataProduk', 'DetailTransaksiKonvenController@getDataProduk')->middleware('isKasir');
      Route::get('transaksi_konven/{id_pembeli}/getDataPembeli', 'DetailTransaksiKonvenController@getDataPembeli')->middleware('isKasir');
      Route::get('transaksi_konven/{nama_pembeli}/getNamaPembeli', 'DetailTransaksiKonvenController@getNamaPembeli')->middleware('isKasir');
      Route::get('searchajax',array('as'=>'searchajax','uses'=>'DetailTransaksiKonvenController@autoComplete'))->middleware('isKasir');
      Route::get('searchajax2','DetailTransaksiKonvenController@autoComplete2')->middleware('isKasir');
      // Route::get('wow',array('as'=>'wow','uses'=>'TransaksiKonvenController@autoComplete2'))->middleware('isKasir');
      Route::post('/autocomplete/fetch', 'DetailTransaksiKonvenController@fetch')->name('autocomplete.fetch')->middleware('isKasir');
      Route::get('qrcode', 'DetailTransaksiKonvenController@generateQrcode')->middleware('isKasir');
      Route::get('invoice/{id}', 'DetailTransaksiKonvenController@getPdf')->middleware('isKasir');
      Route::get('display-search-queries','DetailTransaksiController@searchData')->middleware('isKasir');
