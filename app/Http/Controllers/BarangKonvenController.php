<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use File;
use App\BarangKonven;
use App\Kategori;
use App\PenjualKonven;
use App\User;
use DB;
use Auth;
use Excel;

class BarangKonvenController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = DB::table('barang_konven')
                ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.*', 'kategori.kategori')
                ->where('barang_konven.deleted_at','=',NULL)
                ->get()
                ->toArray();
        return view('barang_konven.list', compact('list'));
    }

    public function index2()
    {
        $user = Auth::user()->penjual_konven()->pluck('id')->first();     
        
        $list = DB::table('barang_konven')
                ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.*', 'kategori.kategori')
                ->where('barang_konven.penjual_konven_id', '=', $user)
                ->where('barang_konven.deleted_at','=',NULL)
                ->get()
                ->toArray();
        return view('barang_konven.list_pos', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        $penjual_konven = PenjualKonven::all();
        $penjual = PenjualKonven::all()->sortBy('nama_toko');
        return view('barang_konven.create', compact('kategori','penjual_konven','penjual'));
    }

    public function create2()
    {
        $kategori = Kategori::all();
        $penjual_konven = User::all();
        return view('barang_konven.create_pos', compact('kategori','penjual_konven'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate(request(),
          [
            'seller' => 'required',
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
            'deskripsi' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'discount' => 'required',
          ],
          [
            'seller.required' => 'Penjual belum dipilih!',
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'deskripsi.required' => 'Deskripsi harus diisi!',
            'price.required' => 'Harga harus diisi!',
            'stock.required' => 'Stok harus diisi!',
            'discount.required' => 'Diskon harus diisi!',
          ]
        );

        if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename='product.png';
         }

        BarangKonven::create([
          'penjual_konven_id'=>request('seller'),
          'kategori_id'=>request('kategori'),
          'nama'=>request('nama'),
          'deskripsi'=>request('deskripsi'),
          'harga'=>request('price'),
          'stok'=>request('stock'),
          'diskon'=>request('discount'),
          'foto'=>$filename
        ]);
      return redirect('master/barang_konven')->with('success','Barang berhasil ditambahkan.');
    }

    public function store2(Request $request)
    {
        $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
            'deskripsi' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'diskon' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'deskripsi.required' => 'Deskripsi harus diisi!',
            'harga.required' => 'Harga harus diisi!',
            'stok.required' => 'Stok harus diisi!',
            'diskon.required' => 'Diskon harus diisi!',
          ]
        );

        if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename='product.png';
         }

        BarangKonven::create([
          'penjual_konven_id'=>Auth::user()->penjual_konven()->pluck('id')->first(),
          'kategori_id'=>request('kategori'),
          'nama'=>request('nama'),
          'deskripsi'=>request('deskripsi'),
          'harga'=>request('harga'),
          'stok'=>request('stok'),
          'diskon'=>request('diskon'),
          'foto'=>$filename
        ]);
      return redirect('barang_konven/list_pos')->with('success','Barang berhasil ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = BarangKonven::findOrFail($id);
      $kategori = Kategori::all()->sortBy('kategori');
      return view('barang_konven.edit',compact('data','kategori'));
    }

    public function edit2($id)
    {
      $data = BarangKonven::findOrFail($id);
      $kategori = Kategori::all()->sortBy('kategori');
      return view('barang_konven.edit_pos',compact('data','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $gambar = DB::table('barang_konven')->where('id', '=', $id)->pluck('foto')->first();
      // dd($gambar);

      $this->validate(request(),
          [
            'seller' => 'required',
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
            'deskripsi' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'discount' => 'required',
          ],
          [
            'seller.required' => 'Penjual belum dipilih!',
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'deskripsi.required' => 'Deskripsi harus diisi!',
            'price.required' => 'Harga harus diisi!',
            'stock.required' => 'Stok harus diisi!',
            'discount.required' => 'Diskon harus diisi!',
          ]
        );

      if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename=$gambar;
         }


      $data = BarangKonven::find($id);
      $data->kategori_id=$request->get('kategori');
      $data->nama=$request->get('nama');
      $data->deskripsi=$request->get('deskripsi');
      $data->harga=$request->get('harga');
      $data->stok=$request->get('stok');
      $data->diskon=$request->get('diskon');
      $data->foto=$filename;

      $data->save();
      return redirect('master/barang_konven')->with('success', 'Data barang berhasil diubah.');
    }

    public function update2(Request $request, $id)
    {

      $gambar = DB::table('barang_konven')->where('id', '=', $id)->pluck('foto')->first();
      // dd($gambar);

      $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
            'deskripsi' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'diskon' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'deskripsi.required' => 'Deskripsi harus diisi!',
            'harga.required' => 'Harga harus diisi!',
            'stok.required' => 'Stok harus diisi!',
            'diskon.required' => 'Diskon harus diisi!',
          ]
        );

      if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename=$gambar;
         }

      $data = BarangKonven::find($id);
      $data['penjual_konven_id']=Auth::user()->penjual_konven()->pluck('id')->first();
      $data->kategori_id=$request->kategori;
      $data->nama=$request->nama;
      $data->diskon=$request->diskon;
      $data->deskripsi=$request->deskripsi;
      $data->stok=$request->stok;
      $data->harga=$request->harga;
      $data->foto=$filename;

      $data->save();
      return redirect('barang_konven/list_pos')->with('success', 'Data barang berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = BarangKonven::find($id)->delete();
      return redirect()->back()->with('success', 'Data berhasil dihapus.');;
    }

    public function delete($id)
    {
        $karyawan = BarangKonven::find($id);
        $karyawan->delete();
 
        return redirect()->back()->with('success', 'Data berhasil di hapus.');
    }

    public function exportFile(Request $request)
    {
            $data = DB::table('barang_konven')
                ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.id','barang_konven.nama','kategori.kategori','barang_konven.deskripsi','barang_konven.harga','barang_konven.stok','barang_konven.diskon')
                ->get()
                ->toArray();

            $data= json_decode( json_encode($data), true);
            Excel::create('DataBarang', function($excel) use($data){
            $excel->sheet('Data Barang', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
    }

    // START OF API Controller

    // Api lihat semua barang dan toko
    public function api_index()
    {
        $barang = BarangKonven::leftJoin('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                          ->select('barang_konven.*','penjual_konven.nama_toko')
                          ->get();
        return response()->json([
          'status'=>'success',
          'result'=>$barang
        ]);
    }

    // API lihat barang pennjual berdasarkan id
    public function api_index_penjual()
    {

        $user = Auth::user()->penjual_konven()->pluck('id')->first();  
        $produk=BarangKonven::where('penjual_konven_id','=',$user)->get()->sortBy('name');
        return response()->json([
          'status'=>'success',
          'result'=>$produk
        ]);
    }

}
