<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembeli;
use App\Topup;
use App\LogSaldo;
use App\User;
use Alert;
use DB;

class TopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::where('level','Pembeli')->get()->sortBy('name');
        return view('topup.find',compact('list'));
    }

    public function detail($id)
    {
        $data = User::find($id);
        $history = LogSaldo::where('user_id', $id)->get();
        // dd($history);
        return view('topup.detail', compact('data','history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
      $data = Pembeli::where('id', $id)->first();
      return view('pembeli-master.tambah-saldo', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      DB::beginTransaction();
      $this->validate($request,
      [
        'total' => 'required',
      ],
      [
        'total.required' => 'Top up field can not be empty!',
      ]);
      $pembeli = User::where('id', $request->user_id)->first();
      $topup = Topup::create([
        'user_id'=>$pembeli->id,
        'total'=>$request->total,
      ]);
      if (!$topup) {
        DB::rollback();
        
        return redirect('payment')->with('Error', 'Ada yang salah!');
      }
      $topup->logSaldo()->create([
          'user_id'=>$pembeli->id,
          'saldo_awal'=>$pembeli->saldo,
          'type'=>'topup',
          'total'=>$request->total,
          'grand_total'=>$pembeli->saldo + $request->total
        ]);
        if (!$topup) {
          DB::rollback();
          return redirect('payment')->with('Error', 'Ada yang salah!');
        }
      $pembeli->saldo = $topup->logSaldo->grand_total;
      $pembeli->save();
      DB::commit();
      
      return redirect()->back()->with('success', 'Saldo berhasil di updated!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
