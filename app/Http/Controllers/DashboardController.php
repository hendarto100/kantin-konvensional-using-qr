<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  
  public function index()
  {
  		$list = DB::table('detail_transaksi_konven')
                ->join('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                ->join('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                ->select('detail_transaksi_konven.*','barang_konven.nama', 'transaksi_konven.id')
                ->orderBy('barang_konven_id', 'DESC')
                ->get()
                ->toArray();

      $produk = DB::table('barang_konven')->count();
      $pembeli = DB::table('pembeli')->count();
      $transaksi = DB::table('transaksi_konven')->count();
      $hari_ini = DB::table('transaksi_konven')->whereBetween('created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])->count();

        $data1 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','1')->count();
        $data2 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','2')->count();
        $data3 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','3')->count();
        $data4 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','4')->count();
        $data5 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','5')->count();
        $data6 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','6')->count();
        $data7 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','7')->count();
        $data8 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','8')->count();  
        $data9 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','9')->count();  
        $data10 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','10')->count();  
        $data11 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','11')->count();  
        $data12 = DB::table('transaksi_konven')->select('id')->whereYear('created_at',date('Y'))->whereMonth('created_at','12')->count();        

        $chart1 = \Chart::title([
        'text' => 'Grafik Transaksi Bulanan',
          ])
          ->chart([
              'type'     => 'line', // pie , columnt ect
              'renderTo' => 'chart1', // render the chart into your div with id
          ])
          ->subtitle([
              'text' => 'Koperasi Kopma UGM',
          ])
          ->colors([
              '#0c2959'
          ])
          ->xaxis([
              'categories' => [
                  'Januari',
                  'Februari',
                  'Maret',
                  'April',
                  'Mei',
                  'Juni',
                  'Juli',
                  'Agustus',
                  'September',
                  'Oktober',
                  'November',
                  'Desember',
              ],
              'labels'     => [
                  'rotation'  => 15,
                  'align'     => 'top',
                  'formatter' => 'startJs:function(){return this.value }:endJs', 
                  // use 'startJs:yourjavasscripthere:endJs'
              ],
          ])
          ->yaxis([
              'text' => 'This Y Axis',
          ])
          ->legend([
              'layout'        => 'vertikal',
              'align'         => 'right',
              'verticalAlign' => 'middle',
          ])
          ->series(
              [
                  [
                      'name'  => 'Jumlah Transaksi',
                      'data'  => [$data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11, $data12],
                      // 'color' => '#0c2959',
                  ],
              ]
          )
          ->display();


      return view('dashboard.index',compact('list','produk','pembeli','transaksi','hari_ini','chart1'));
  }
}
