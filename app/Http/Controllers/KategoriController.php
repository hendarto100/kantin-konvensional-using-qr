<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Barang;

class KategoriController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Kategori::all()->sortBy('kategori');
        return view('kategori.list',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate(request(),
        [
          'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
          'deskripsi' => 'required',
        ],
        [
          'nama.required' => 'Category can not be empty!',
          'nama.regex' => 'Category can not be numeric only!',
          'deskripsi.required' => 'Description can not be empty!',
        ]
      );
      Kategori::create([
            'kategori' => request('nama'),
            'deskripsi'=>request('deskripsi')
      ]);
      
      return redirect('master/kategori')->with('success','Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data= Kategori::find($id);
      return view('kategori.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
      $this->validate(request(),
      [
        'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
        'deskripsi' => 'required',
      ],
      [
        'nama.required' => 'Category can not be empty!',
        'nama.regex' => 'Category can not be numeric only!',
        'deskripsi.required' => 'Description can not be empty!',
      ]);

      $data= Kategori::find($id);
      $data->kategori=$request->nama;
      $data->deskripsi=$request->deskripsi;

      $data->save();

      return redirect('master/kategori')->with('success', 'Data berhasil diperbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // remove id from KATEGORI
      $data = Kategori::find($id)->delete();
      // remmove kategori_id from BARANG
      // $kategori_id = Barang::where('kategori_id', $id)->delete();
      // remove from barang_id from BARANGJUAL
      // $barang_id = BarangJual::where('barang_id', $kategori_id->id)->delete();
      return response()->json($data);
      // return redirect('master/kategori')->with('alert', 'Data has been deleted!');
    }

    public function withTrashed()
    {
      $data = Kategori::onlyTrashed()
                ->get();
      dd($data);
    }

    public function restore($id)
    {
      Kategori::withTrashed()->where('id',$id)->restore();
          return "success restore";
    }
    public function forceDelete($id)
    {
      $data = Kategori::onlyTrashed($id)->first()->forceDelete();;
      return "success force delete";
    }
}
