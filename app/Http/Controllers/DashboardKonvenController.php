<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class DashboardKonvenController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  
  public function index()
  {
  		date_default_timezone_set('Asia/Jakarta');

      $user = Auth::user()->penjual_konven()->pluck('id')->first(); 

  		$produk = DB::table('barang_konven')->where('barang_konven.penjual_konven_id', '=', $user)->count();
  		$pembeli = DB::table('pembeli')->count();
  		$transaksi = DB::table('transaksi_konven')->where('transaksi_konven.penjual_konven_id', '=', $user)->count();
  		$hari_ini = DB::table('transaksi_konven')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereBetween('created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])->count();
      $saldo_hari_ini = DB::table('transaksi_konven')->where('status', '=', 'Lunas')->whereBetween('created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])->sum('total');

        $data1 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','1')->count();
        $data2 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','2')->count();
        $data3 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','3')->count();
        $data4 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','4')->count();
        $data5 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','5')->count();
        $data6 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','6')->count();
        $data7 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','7')->count();
        $data8 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','8')->count();  
        $data9 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','9')->count();  
        $data10 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','10')->count();  
        $data11 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','11')->count();  
        $data12 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','12')->count();        

        $chart1 = \Chart::title([
        'text' => 'Daftar Transaksi Bulanan',
          ])
          ->chart([
              'type'     => 'line', // pie , columnt ect
              'renderTo' => 'chart1', // render the chart into your div with id
          ])
          ->subtitle([
              'text' => 'Koperasi Kopma UGM',
          ])
          ->colors([
              '#0c2959'
          ])
          ->xaxis([
              'categories' => [
                  'Januari',
                  'Februari',
                  'Maret',
                  'April',
                  'Mei',
                  'Juni',
                  'Juli',
                  'Agustus',
                  'September',
                  'Oktober',
                  'November',
                  'Desember',
              ],
              'labels'     => [
                  'rotation'  => 15,
                  'align'     => 'top',
                  'formatter' => 'startJs:function(){return this.value }:endJs', 
                  // use 'startJs:yourjavasscripthere:endJs'
              ],
          ])
          ->yaxis([
              'text' => 'This Y Axis',
          ])
          ->legend([
              'layout'        => 'vertikal',
              'align'         => 'right',
              'verticalAlign' => 'middle',
          ])
          ->series(
              [
                  [
                      'name'  => 'Jumlah Transaksi',
                      'data'  => [$data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11, $data12],
                      // 'color' => '#0c2959',
                  ],
              ]
          )
          ->display();

      	return view('dashboard_konven.dashboard',compact('produk','pembeli','transaksi','hari_ini','chart1','saldo_hari_ini'));
  }
}
