<?php

namespace App\Http\Controllers;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Http\Request;
use App\User;
use App\PembeliMaster;
use App\Pembeli;
use App\Status;
use Carbon\Carbon;
Use Alert;
Use Auth;
use DB;

class PembeliMasterController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $list = Pembeli::leftJoin('users', 'pembeli.user_id', '=', 'users.id')
        //                 ->select('pembeli.id', 'pembeli.user_id', 'pembeli.nama', 'users.email','users.no_telepon','users.status_id')->get();

        $list = User::leftJoin('pembeli', 'users.id', '=', 'pembeli.user_id')->select('pembeli.id','pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')->where('users.level','Pembeli')->where('users.name','!=','Pembeli')->get();
        return view('pembeli-master.list', compact('list'));
    }

    public function index2()
    {

      $list = Pembeli::select('id', 'nama')->where('member','=','Tidak')->get();
        // $list = User::leftJoin('pembeli', 'users.id', '=', 'pembeli.user_id')->select('pembeli.id', 'users.name', 'users.email','users.no_telepon','users.status_id')->where('users.level','Pembeli')->where('users.name','!=','Pembeli')->where('pembeli.member','=','Ya')->get();
        return view('pembeli-master.list_bukan_member', compact('list'));
    }
    public function aktif()
    {
        $list = Pembeli::leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                        ->select('pembeli.id', 'pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 1)
                        ->get();
        return view('pembeli-master.list', compact('list'));
    }
    public function nonAktif()
    {
        $list = Pembeli::leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                        ->select('pembeli.id', 'pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 2)
                        ->get();
        return view('pembeli-master.list', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::all();
        return view('pembeli-master.create', compact('status'));
    }


    public function create2($id)
    {
        $data = Pembeli::where('id', $id)->first();
        $nama = Pembeli::find($id)->pluck('nama')->first();
        $status = Status::all();
        return view('pembeli-master.create_nonmember', compact('status','data','nama'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
  {
    $this->validate(request(),
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|unique:users',
        'password' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'no_telepon' => 'required|unique:users',
        'tanggal_lahir' => 'required',
        // 'status_id' => 'required',
        'saldo' => 'required',
      ],
      [
        'name.required' => 'Name can not be empty!',
        'name.regex' => 'Name can not be numeric only!',
        'name.unique' => 'Name has already been taken!',
        'password.required' => 'Password can not be empty!',
        'password.min' => ' Password must be at least 8 characters!',
        'email.required' => 'Email can not be empty!',
        'email.email' => 'Email must be a valid email address!',
        'email.unique' => 'Email has already been taken!',
        'no_telepon.required' => 'Phone number can not be empty!',
        'no_telepon.unique' => 'Phone number has already been taken!',
        'tanggal_lahir.required' => 'Born day can not be empty!',
        // 'status_id.required' => 'Status tidak boleh kosong!',
        'saldo.required' => 'Saldo can not be empty!'
      ]);

      if(!empty($request->foto)){
         $file = $request->file('foto');
         $extension = strtolower($file->getClientOriginalExtension());
         $filename = $request->name . '.' . $extension;
         Storage::put('images/' . $filename, File::get($file));
         $file_server = Storage::get('images/' . $filename);
         $img = Image::make($file_server)->resize(141, 141);
         $img->save(base_path('public/images/' . $filename));
       }

      $user = User::create([
        'level'=>'Pembeli',
        'name'=>$request->name,
        'password'=>bcrypt($request->password),
        'email'=>$request->email,
        'no_telepon'=>$request->no_telepon,
        'tanggal_lahir'=>$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir),
        'status_id'=>2,
        'saldo'=>$request->saldo,
      ])
      ->pembeli()->create([
        'nama'=>$request->name,

      ]);
      if (!empty($request->foto)) {
        $user->user->foto=$filename;
        // dd($user->foto);
        $user->user->save();
      }else {
        $user->user->foto='avatar.png';
        $user->user->save();
      }

      return redirect('master/pembeli')->with('success','Member berhasil ditambahkan.');
  }

  public function store2($id, Request $request)
  {
      $pembeli = Pembeli::where('id', $id)->first();
      $data= User::where('id',$pembeli->user_id)->first();
      $this->validate(request(),
      [
        'password' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'no_telepon' => 'required|unique:users',
        'tanggal_lahir' => 'required',
        // 'status_id' => 'required',
        'saldo' => 'required',
      ],
      [
        'password.required' => 'Password can not be empty!',
        'password.min' => ' Password must be at least 8 characters!',
        'email.required' => 'Email can not be empty!',
        'email.email' => 'Email must be a valid email address!',
        'email.unique' => 'Email has already been taken!',
        'no_telepon.required' => 'Phone number can not be empty!',
        'no_telepon.unique' => 'Phone number has already been taken!',
        'tanggal_lahir.required' => 'Born day can not be empty!',
        // 'status_id.required' => 'Status tidak boleh kosong!',
        'saldo.required' => 'Saldo can not be empty!'
      ]);

      // $jajal = $request->get('name');
      // dd($jajal);

      if(!empty($request->foto)){
         $file = $request->file('foto');
         $extension = strtolower($file->getClientOriginalExtension());
         $filename = $request->get('name') . '.' . $extension;
         Storage::put('images/' . $filename, File::get($file));
         $file_server = Storage::get('images/' . $filename);
         $img = Image::make($file_server)->resize(141, 141);
         $img->save(base_path('public/images/' . $filename));
       }

      $user = User::create([
        'level'=>'Pembeli',
        'name'=>$request->name,
        'password'=>bcrypt($request->password),
        'email'=>$request->email,
        'no_telepon'=>$request->no_telepon,
        'tanggal_lahir'=>$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir),
        'status_id'=>2,
        'saldo'=>$request->saldo,
      ]);

      $pembeli->user_id=DB::getPdo()->lastInsertId();
      $pembeli->member='Ya';
      $pembeli->save();

      if (!empty($request->foto)) {
        $user->foto=$filename;
        // dd($user->foto);
        $user->save();
      }else {
        $user->foto='avatar.png';
        $user->save();
      }

      return redirect('master/pembeli_bukan_member')->with('success','Member berhasil ditambahkan.');
  }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $data = Pembeli::where('id', $id)->first();
         $status = Status::all();
         return view('pembeli-master.edit', compact('data','status'));
        //  dd($data);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pembeli = Pembeli::where('id', $id)->first();
      $pembeli->nama=$request->name;
      $data= User::where('id',$pembeli->user_id)->first();
      $this->validate($request,
      [
        'name' => 'required',
        'no_telepon' => 'required',
        'tanggal_lahir' => 'required',
        // 'saldo' => 'required',
      ],
      [
        'name.required' => 'Name can not be empty!',
        'no_telepon.required' => 'Phone number can not be empty!',
        'tanggal_lahir.required' => 'Born day can not be empty!',
        // 'no_telepon.unique' => 'Phone number has already been taken!',
        // 'saldo.required' => 'Saldo can not be empty!'
      ]);
      if ($data->name != $request->name) {
        $this->validate($request,
        [
          'name' => 'unique:users',
        ]);
      }
      if ($data->no_telepon != $request->no_telepon) {
        $this->validate($request,
        [
          'no_telepon' => 'unique:users',
        ],
        [
          'no_telepon.unique' => 'Phone number has already been taken.',
        ]);
      }
      $data->name=$request->name;
      $data->email=$request->email;
      $data->no_telepon=$request->no_telepon;
      $data->tanggal_lahir=$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir);

      // $check_name = User::where('name', $request->name)->get()->count();
      // if($check_name == 1)

      $data->save();
      $pembeli->save();
      
      return redirect('master/pembeli/'.$pembeli->id.'');
    }

    public function editPassword($id)
    {
        $data = Pembeli::where('id', $id)->first();
        return view('pembeli-master.edit-password', compact('data'));
       //  dd($data);
    }
    public function updatePassword(Request $request, $id)
    {
      $pembeli = Pembeli::where('id', $id)->first();
      $data= User::where('id',$pembeli->user_id)->first();
      $data->password=bcrypt($request->password);

      $this->validate($request,
        [
          'password' => 'required|min:8',
        ],
        [
          'password.required' => 'Password can not be empty!',
          'password.min' => ' Password must be at least 8 characters!',
        ]);

      $data->save();
      Alert::success('Success', 'Password has been changed!');
      return redirect('master/pembeli/'.$pembeli->id.'');
      // ->with('alert', 'Password has been changed!');
      // dd($data);
    }
    public function topUp($id)
    {
        $data = Pembeli::where('id', $id)->first();
        return view('pembeli-master.tambah-saldo', compact('data'));
       //  dd($data);
    }
    public function topUpStore(Request $request, $id)
    {
      $pembeli = Pembeli::where('id', $id)->first();
      $pembeli->saldo = $pembeli->saldo + $request->topup;

      $this->validate($request,
        [
          'topup' => 'required|',
        ],
        [
          'topup.required' => 'Top up field can not be empty!',
        ]);

      $pembeli->save();
      Alert::success('Success', 'Saldo has been updated!');
      return redirect('master/pembeli/'.$pembeli->id.'');
      // ->with('alert', 'Password has been changed!');
      // dd($data);
    }
    public function editAvatar($id)
    {
        $data = Pembeli::find($id);
        return view('pembeli-master.edit-ava', compact('data'));
    }
    public function updateAvatar(Request $request, $id)
    {
      $pembeli = Pembeli::find($id);
      $data= User::where('id',$pembeli->user_id)->first();

      $this->validate($request,
      [
        'foto' => 'required',
      ],
      [
        'foto.required' => 'Avatar can not be empty!',
      ]);

      $file = $request->file('foto');
      $extension = strtolower($file->getClientOriginalExtension());
      $filename = $data->name . '.' . $extension;
      Storage::put('images/' . $filename, File::get($file));
      $file_server = Storage::get('images/' . $filename);
      $img = Image::make($file_server)->resize(141, 141);
      $img->save(base_path('public/images/' . $filename));

      $data->foto=$filename;
      $data->save();
      Alert::success('Success', 'Password has been changed!');
      return redirect('master/pembeli/'.$pembeli->id.'');
    }

    public function status(Request $request, $user_id)
    {

      $data= User::find($user_id);
      if($data['status_id']==1){
        $data->status_id=2;
      }
      else{
        $data->status_id=1;
      }
      $data->save();
      
      return redirect()->back();
      // ->with('alert', 'Status has been changed!');
    }
    public function detail($id)
    {
        $data = Pembeli::find($id);
        return view('pembeli-master.detail', compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Pembeli::find($id)->delete();
      return response()->json($data);
      // return redirect()->back()->with('alert', 'Data has been deleted!');
    }
}
