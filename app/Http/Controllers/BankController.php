<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
Use Alert;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
        //  $this->middleware('auth');
     }
       /**
        * Display a listing of the resource.
        *
        * @return \Illuminate\Http\Response
        */
       public function index()
       {
           $list = Bank::all()->sortBy('nama');
           return view('bank.list', compact('list'));
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
       public function create()
       {
           return view('bank.create');
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
       public function store(Request $request)
       {
           $this->validate(request(),
             [
               'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
               'deskripsi' => 'required',
             ],
             [
               'nama.required' => 'Bank cant be empty!',
               'nama.regex' => 'Bank can not be numeric only!',
               'deskripsi.required' => 'Description can not be empty!',
             ]
           );

           Bank::create([
             'nama'=>request('nama'),
             'deskripsi'=>request('deskripsi')
           ]);
           Alert::success('Success', 'Data saved successfully!');
         return redirect('master/bank');
        //  ->with('alert','Data saved successfully!');
       }

       /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function show($id)
       {
           //
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function edit($id)
       {
         $data = Bank::findOrFail($id);
         return view('bank.edit',compact('data'));
       }

       /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function update(Request $request, $id)
       {
         $data = Bank::find($id);
         $data->nama=$request->get('nama');
         $data->deskripsi=$request->get('deskripsi');

         $this->validate(request(),
         [
           'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
           'deskripsi' => 'required',
         ],
         [
           'nama.required' => 'Bank cant be empty!',
           'nama.regex' => 'Bank can not be numeric only!',
           'deskripsi.required' => 'Description can not be empty!',
         ]
         );

         $data->save();
         Alert::success('Success', 'Data has been updated!');
         return redirect('master/bank');
        //  ->with('alert', 'Data has been updated!');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function destroy($id)
       {
         $data = Bank::where('id',$id)->delete();
        //  return redirect('master/bank')->with('alert', 'Data has been deleted!');
         return response()->json($data);
       }

       public function withTrashed()
       {
         $data = Bank::onlyTrashed()
                   ->get();
         dd($data);
       }

       public function restore($id)
       {
         Bank::withTrashed()->where('id',$id)->restore();
             return "success restore";
       }
       public function forceDelete($id)
       {
         $data = Bank::onlyTrashed($id)->first()->forceDelete();;
         return "success force delete";
       }
}
