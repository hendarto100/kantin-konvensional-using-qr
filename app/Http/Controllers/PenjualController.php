<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PenjualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::where('level','Penjual')->get();
        return view('penjual.list', compact('list'));

        // dd($list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penjual.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      User::create([
            'level' => 'Penjual',
            'name' => request('username'),
            'email' => request('email'),
            'no_telepon' => request('no_telephone'),
            'password' => request('password'),
            'tanggal_lahir' => request('tanggal_lahir')
      ]);
      return redirect('penjual')->with('success','Penjual berhasil disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('penjual.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data= User::find($id);
      $data->username=$request->username;
      $data->email=$request->email;
      $data->no_telephone=$request->no_hp;
      $data->password=$request->password;
      $data->tanggal_lahir=$request->tanggal_lahir;
      $data->save();
      return redirect('penjual');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = User::find($id)->delete();
      return redirect('penjual');
    }
}
