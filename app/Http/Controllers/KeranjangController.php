<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keranjang;
use Auth;
use App\User;

class KeranjangController extends Controller
{
  public function api_index()
  {
    $penjual = User::find(Auth::user()->id)->penjual_konven()->first()->id;
    $data = Keranjang::where('penjual_konven_id', $penjual)->with('barang_konven')->get();
    if (empty($data)) {
      return response()->json([
        'status'=>'success',
        'result'=> 'chart is empty',
      ]);
    }
    return response()->json([
      'status'=>'success',
      'result'=> $data,
    ]);
  }

  public function api_store(Request $request, $id)
  {
      $penjual = User::find(Auth::user()->id)->penjual_konven()->first()->id;
      $checkKeranjang = Keranjang::where('barang_konven_id', $id)->first();
      // dd($checkKeranjang);
      if ($checkKeranjang){
        $data = $checkKeranjang;
        $data->kuantitas+=$request->kuantitas;
        $data->save();
      }
      else {
        $data = Keranjang::create([
          'penjual_konven_id'=>$penjual,
          'barang_konven_id'=>$id,
          'kuantitas'=>$request->kuantitas
        ]);
      }
      return response()->json([
        'status'=>'success',
        'result'=>$data
      ]);
  }

  public function api_update(Request $request, $id)
  {
        $data = Keranjang::where('id', $id)->first();
        // dd($data);
        $data->kuantitas=$request->kuantitas;
        $data->save();

        return response()->json([
          'status'=>'success',
          'result'=>$data
        ]);
  }

  public function api_destroy($id)
  {
      $data = Keranjang::find($id)->delete();
      return response()->json([
        'status'=>'data has been deleted',
      ]);
  }
  public function api_destroyAll()
  {
      $pembeli = User::find(Auth::user()->id)->pembeli()->first()->id;
      $data = Keranjang::where('pembeli_id', $pembeli)->delete();
      return response()->json([
        'status'=>'all item has been deleted',
      ]);
  }
}
