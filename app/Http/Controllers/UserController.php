<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Hash;
use App\PenjualKonven;
use App\Pembeli;
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{
  public function __construct()
  {
      //
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    // START OF API CONTROLLER

  public function api_registerPembeli(Request $request)
   {
     $request->validate([
       'name' => 'required|string|unique:users',
       'email' => 'required|string|email|unique:users',
       'password' => 'required|string',
       'no_telepon' => 'required|unique:users',
     ]);
     $user = User::create([
       'level'=>'Pembeli',
       'name'=>$request->name,
       'password'=>bcrypt($request->password),
       'email'=>$request->email,
       'no_telepon'=>$request->no_telepon,
       'tanggal_lahir'=>$request->tanggal_lahir,
       'status_id'=>$request->status_id
     ])
      ->pembeli()->create([
      ]);

    $pembeli = User::where('id', $user->user_id)->with('pembeli')->first();
    // dd($pembeli);
     return response()->json([
       'status'=>'success',
       'user'=>$pembeli
     ]);
   }
   public function api_loginPembeli(Request $request){
      if(Auth::attempt([
          'email' => request('email'),
          'password' => request('password'),
          'level' => 'Pembeli'
      ])){
          $user = Auth::user();
          $email = $request->get('email');
          $password = $request->get('password');
          $success['token'] =  $user->createToken('MyApp')-> accessToken;
          $success['email'] = $email;
          $success['password'] = $password;

          $pembeli = User::where('email', $success['email'])->with('pembeli')->first();
          $pembeli->token = $success['token'];
          // dd($pembeli);
          return response()->json([
              // 'error'=>false,
              'status'=>'success',
              // 'token' => $success['token'],
              'user' => $pembeli
          ]);
      }
      else{
          $success['status'] = 'failed';
          $success['error'] = 'Unauthorised';
          $success['message'] = 'Your email or password incorrect!';
          return response()->json([$success],401);
      }
  }

  public function api_loginPenjual(Request $request){
     if(Auth::attempt([
         'email' => request('email'),
         'password' => request('password'),
         'level' => 'Kasir'
     ])){
         $user = Auth::user();
         $email = $request->get('email');
         $password = $request->get('password');
         $success['token'] =  $user->createToken('MyApp')-> accessToken;
         $success['email'] = $email;
         $success['password'] = $password;

         $pembeli = User::where('email', $success['email'])->with('penjual_konven')->first();
         $pembeli->token = $success['token'];
         // dd($pembeli);
         return response()->json([
            'error'=>false,
             'status'=>'success',
             // 'token' => $success['token'],
             'user' => $pembeli
         ]);
     }
     else{
         $success['status'] = 'failed';
         $success['error'] = 'Unauthorised';
         $success['message'] = 'Your email or password incorrect!';
         return response()->json([$success],401);
     }
 }

 public function api_logout(Request $request)
 {
   dd($request->user());
   $request->user()->token()->revoke();
   return response()->json([
     'message' => 'Successfully logged out'
   ]);
 }

  public function api_detail()
  {
    $users = User::with('penjual_konven', 'pembeli')->where('id', Auth::user()->id)->first();
    return response()->json([
      'status'=>'success',
      'result'=> $users
    ]);
  }

  public function api_updatePembeli(Request $request)
  {
    $data= User::where('id', Auth::user()->id)->first();
    // dd($users);
    // $data->name=$request->name;
    $data->email=$request->email;
    $data->no_telepon=$request->no_telepon;
    $data->tanggal_lahir=$request->tanggal_lahir;
    $this->validate($request,
      [
        // 'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
        'email' => 'required|string|email|unique:users',
        'no_telepon' => 'required',
        'tanggal_lahir' => 'required',
      ]);
    $data->save();
    return response()->json([
      'status'=>'success',
      'user'=>$data
    ]);
  }
  public function api_updatePenjual(Request $request)
  {
    $penjual = Penjual::where('user_id',Auth::user()->id)->first();
    $penjual->no_rekening=$request->no_rekening;
    $penjual->bank_id=$request->bank_id;
    $data= User::where('id',Auth::user()->id)->first();
    $data->email=$request->email;
    $data->no_telepon=$request->no_telepon;
    $data->tanggal_lahir=$request->tanggal_lahir;
    $this->validate($request,
      [
        'email' => 'required|string|email|unique:users',
        'no_telepon' => 'required',
        'tanggal_lahir' => 'required',
      ]);
    $penjual->save();
    $data->save();
    return response()->json([
      'status'=>'success',
      'user'=>$data,
      'penjual'=>$penjual
    ]);
  }
  public function api_updatePassword(Request $request)
  {
    $data= User::find(Auth::user()->id);
    if(Hash::check($request->password_current,$data->password)){
      $data->password = Hash::make($request->password);
      $this->validate($request,
        [
          'password_current' => 'required',
          'password' => 'required|string|min:8|confirmed',
          'password_confirmation' => 'required',
          // 'password_confirmation' => 'required|string|min:8|confirmed',
        ],
        [
          // 'name.required' => 'Name can not be empty!',
        ]);
      $data->save();
      return response()->json([
        'status'=>'success',
        'user'=>$data
      ]);
    }
    else
    {
      return response()->json([
        'status'=>'Failed',
        // 'user'=>$data
      ]);
    }
  }

  public function api_uploadAva(Request $request)
  {
    $data= User::find(Auth::user()->id);
    $path = base_path().'/public/images/';
    $photo = Helper::uploadPhoto($request->foto,$path);
    $data->foto = $photo['image_name'];
    dd($data);
    $data->save();
  }
}
