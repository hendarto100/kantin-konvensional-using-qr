<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransaksiKonven;
use App\DetailTransaksiKonven;
use DB;
use App\User;
use App\Pembeli;
use App\PenjualKonven;
use App\BarangKonven;
use App\Keranjang;
use Auth;
use Excel;

class TransaksiKonvenController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->penjual_konven()->pluck('id')->first();

        $list = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->select('transaksi_konven.*','jenis_pembayaran.jenis_pembayaran','pembeli.nama')
                ->where('penjual_konven_id','=',$user)
                ->get();

        return view('transaksi_koven.list', compact('list'));
    }

    public function showDetail($id)
    {
        $list = DB::table('detail_transaksi_konven')
                ->join('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                ->select('detail_transaksi_konven.*','barang_konven.nama')
                ->where('transaksi_konven_id', $id)
                ->get();
        return view('transaksi_koven.list_detail', compact('list'));
    }

    // public function autoComplete2(Request $request) {
    //     $query = $request->get('term','');

    //     $products=Pembeli::where('nama','LIKE','%'.$query.'%')->get();
        
    //     $data=array();
    //     foreach ($products as $product) {
    //         $data[]=array('value'=>$product->nama,'id'=>$product->id);
    //     }
    //     if(count($data))
    //         return $data;
    //     else
    //         return ['value'=>'Hasil tidak ditemukan','id'=>''];
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportFile(Request $request)
    {

        $path1 = $request->get('tgl_awal');
        $path = $request->get('tgl_akhir');
        $user = Auth::user()->penjual_konven()->pluck('id')->first();   
        // $nama = 'laporan_transaksi_'.date('Y-m-d_H-i-s');
        if(!empty($path1) && !empty($path)){

            $user = Auth::user()->penjual_konven()->pluck('id')->first();

            $data = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->select('transaksi_konven.id','pembeli.nama','jenis_pembayaran.jenis_pembayaran','transaksi_konven.status','transaksi_konven.diskon','transaksi_konven.total','transaksi_konven.created_at')
                ->where('penjual_konven_id','=',$user)
                ->whereBetween('transaksi_konven.updated_at',[$path1,$path])
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('DataBarang', function($excel) use($data){
            $excel->sheet('Data Barang', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
        }

        else{

            $data = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->select('transaksi_konven.id','pembeli.nama','jenis_pembayaran.jenis_pembayaran','transaksi_konven.status','transaksi_konven.diskon','transaksi_konven.total','transaksi_konven.created_at')
                ->where('penjual_konven_id','=',$user)
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('DataBarang', function($excel) use($data){
            $excel->sheet('Data Barang', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");

        }
    }


    // START OF API Controller
    public function api_index()
    {
      $transaksi = TransaksiKonven::with('pembeli','jenisPembayaran','detailTransaksiKonven')->get();
      return response()->json([
        'status'=>'success',
        'result'=> $transaksi,
      ]);
    }

     public function api_store(Request $request)
     {
       DB::beginTransaction();
       if (Keranjang::where('penjual_konven_id', User::find(Auth::user()->id)->penjual_konven()->first()->id)) {
         $penjual = User::find(Auth::user()->id)->penjual_konven()->first()->id;
         $keranjang = Keranjang::leftJoin('barang_konven', 'keranjang.barang_konven_id', '=', 'barang_konven.id')
                            ->where('keranjang.penjual_konven_id', $penjual)
                            ->select('keranjang.penjual_konven_id','barang_konven.stok','barang_konven.id','barang_konven.diskon as diskon','kuantitas','barang_konven.harga as harga')
                            ->get()->toArray();

         $diskon = Keranjang::leftJoin('barang_konven', 'keranjang.barang_konven_id', '=', 'barang_konven.id')
                            ->where('keranjang.penjual_konven_id', $penjual)
                            ->select('keranjang.penjual_konven_id','barang_konven.stok','barang_konven.id','barang_konven.diskon as diskon','kuantitas','barang_konven.harga as harga')
                            ->get()->toArray();
          // $diskon = Keranjang::leftJoin('barang_konven', 'keranjang.barang_konven_id', '=', 'barang_konven.id')
          //                   ->where('keranjang.penjual_konven_id', $penjual)
          //                   ->sum('barang_konven.harga');
          

         $detail_transaksi = Keranjang::leftJoin('barang_konven', 'keranjang.barang_konven_id', '=', 'barang_konven.id')
                            ->where('keranjang.penjual_konven_id', $penjual)
                            ->select('barang_konven_id','kuantitas','barang_konven.harga as harga','barang_konven.diskon as diskon')
                            ->get()->toArray();
        
        $harga_total = 0;
        $diskon = 0;

        foreach ($keranjang as $key => $value) {

        $keranjang[$key]['diskon'] = $keranjang[$key]['diskon'] * $keranjang[$key]['kuantitas'];
        $keranjang[$key]['harga'] = $keranjang[$key]['harga'] * $keranjang[$key]['kuantitas'];

        $diskon += $keranjang[$key]['diskon'];
        $harga_total += $keranjang[$key]['harga'];

        }

        $harga =  $harga_total - $diskon;
        // dd($harga);        

        $transaksi['penjual_konven_id']=Auth::user()->penjual_konven()->pluck('id')->first();
        $transaksi['pembeli_id']=$request->pembeli_id;
        $transaksi['jenis_pembayaran_id']='2';
        $transaksi['status']='Menunggu';
        $transaksi['diskon']=$diskon;
        $transaksi['total']=$harga;
        $transaksi['created_at']=date('Y-m-d H:i:s');
        $transaksi['updated_at']=date('Y-m-d H:i:s');
        $getId=TransaksiKonven::insertGetId($transaksi);

        foreach ($detail_transaksi as $key => $value) {

        $val = [
          'transaksi_konven_id'=>$getId,
          'barang_konven_id'=>json_encode($value['barang_konven_id']),
          'harga'=>json_encode($value['harga']), 
          'diskon'=>json_encode($value['diskon']),
          'jumlah'=>json_encode($value['kuantitas']),
          'total'=>json_encode($value['harga']*$value['kuantitas'])
        ];

        $jumlah=$value['kuantitas'];

        $detail = DetailTransaksiKonven::create($val);
        BarangKonven::where('id', $value['barang_konven_id'])->decrement('stok', $jumlah);
        }

        $trans = TransaksiKonven::find($getId);  


         $delKeranjang = Keranjang::where('penjual_konven_id', $penjual)->delete();
         DB::commit();
         return response()->json([
           'status'=>'success',
           'result'=>$trans
         ]);
       }
       else
       {
         return response()->json([
           'status'=>'success',
           'result'=>'chart is empty'
         ]);
       }
     }

     public function api_pay(Request $request, $id)
     {
       DB::beginTransaction();

       $trans= TransaksiKonven::find($id);
       // dd($trans);
       $detailTrans = DetailTransaksiKonven::where('transaksi_konven_id', $id)->get();
       // return response()->json([
       //   'status'=>$detailTrans
       // ]);
       // dd($trans->status);
       $pembeli= User::where('id', Auth::user()->id)->first();
       $penjual_konven = User::where('id', $trans->penjual_konven->user->id)->first();

       $saldo = User::where('id', $trans->penjual_konven->user->id)->pluck('saldo')->first();

      if($trans->status == 'Lunas') {
        return response()->json([
          'status'=>'failed',
          'error'=>'Transaksi sudah dibayar',
          'message'=>'Transaksi sudah dibayar',
        ]);
      }
      elseif($trans->status == 'Gagal') {
        return response()->json([
          'status'=>'failed',
          'error'=>'Transaksi gagal',
          'message'=>'Transaksi gagal',
        ]);
      }
      elseif ($trans->status == 'Menunggu') {
        // PAYMENT MENGGUNAKAN PAY-JUR
        // if ($request->jenis_pembayaran_id == 2) {
          if($pembeli->saldo < $trans->jumlah_bayar) {
            return response()->json([
              'status'=>'failed',
              'error'=>'Saldo tidak cukup',
              'message'=>'Saldo tidak cukup',
            ]);
          }
          $trans->jenis_pembayaran_id= 2;
          $trans->status='Lunas';
          $trans->save();
          if (!$trans) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Terjadi kesalahan!',
              'message'=>'Terjadi kesalahan!',
            ]);
          }
          $trans->logSaldo()->create([
            'user_id'=>$pembeli->id,
            'saldo_awal'=>$pembeli->saldo,
            'type'=>'Pembelian',
            'total'=>$trans->total,
            'grand_total'=>$pembeli->saldo - $trans->total
          ]);
          if (!$trans) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Something wrong!',
              'message'=>'Something wrong!',
            ]);
          }
          $pembeli->saldo = $trans->logSaldo->grand_total;
          $pembeli->save();
          if (!$pembeli) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Something wrong!',
              'message'=>'Something wrong!',
            ]);
          }

          $trans->logSaldo()->create([
            'user_id'=>$penjual_konven->id,
            'saldo_awal'=>$penjual_konven->saldo,
            'type'=>'Penjualan',
            'total'=>$trans->total,
            'grand_total'=>$penjual_konven->saldo + $trans->total
          ]);
          if (!$penjual_konven) {
              DB::rollback();
              return response()->json([
                'status'=>'failed',
                'error'=>'Something wrong!',
                'message'=>'Something wrong!',
              ]);
            }

            $penjual_konven->saldo = $saldo+$trans->total;
            $penjual_konven->save();
            if (!$penjual_konven) {
              DB::rollback();
              return response()->json([
                'status'=>'failed',
                'error'=>'Something wrong!',
                'message'=>'Something wrong!',
              ]);
            }

          DB::commit();
          return response()->json([
            'status'=>'success',
            'pembeli'=>$pembeli,
            'penjual_konven'=>$penjual_konven,
            'result'=>$trans,
          ]);

        // PAYMENT MENGGUNAKAN SELAIN PAY-JUR
        // else {
        //   $trans->jenis_pembayaran_id = $request->jenis_pembayaran_id;
        //   $trans->status='Waiting';
        //   $trans->save();
        //   DB::commit();
        //   return response()->json([
        //     'status'=>'success',
        //     'transaksi'=>$trans,
        //     'pembeli'=>$pembeli
        //   ]);
        // }
      }
  }

// Panggil fungsi ini ketika pembayar pakai tap, dan sudah 10 menit
   public function api_transactionFailed($id){
     $trans= Transaksi::find($id);
     $trans->status='Failed';
     // dd($trans->detailTransaksi);
     foreach ($trans->detailTransaksi as $key => $value) {
       $value->logStok()->create([
         'stok_barang_id'=>$value->stok_barang_id,
         'stok_awal'=>$value->stokBarang->stok,
         'type'=>'failed',
         'total'=>$value->kuantitas,
         'stok_update'=>$value->stokBarang->stok + $value->kuantitas
       ]);
       $stokBarang = StokBarang::where('id', $value->stokBarang->id)->first();
       $stokBarang->stok = $stokBarang->stok + $value->kuantitas;
       $stokBarang->save();
     }
     $trans->save();
     return response()->json([
       'status'=>'success',
       'result'=>$trans,
     ]);
   }

// Panggil fungsi ini ketika pembayar pakai cash, dan konfirmasi jika sudah membayar
   public function api_transactionSuccess($id){
     DB::beginTransaction();
     $detailTrans = DetailTransaksi::where('transaksi_id', $id)->get();
     $trans= Transaksi::find($id);
     $trans->status='Success';
     $trans->save();
     if (!$trans) {
       DB::rollback();
       return response()->json([
         'status'=>'failed',
         'error'=>'Something wrong!',
         'message'=>'Something wrong!',
       ]);
     }
     foreach ($detailTrans as $key => $value) {
       $value->logSaldo()->create([
         'user_id'=>$value->stokBarang->barangJual->penjual->user->id,
         'saldo_awal'=>$value->stokBarang->barangJual->penjual->user->saldo,
         'type'=>'sales',
         'total'=>$value->total_bayar,
         'grand_total'=>$value->stokBarang->barangJual->penjual->user->saldo + $value->total_bayar
       ]);
       $penjual = User::where('id', $value->stokBarang->barangJual->penjual->user->id)->first();
       $penjual->saldo += $value->total_bayar;
       $penjual->save();
       if (!$penjual) {
         DB::rollback();
         return response()->json([
           'status'=>'failed',
           'error'=>'Something wrong!',
           'message'=>'Something wrong!',
         ]);
       }
     }
     DB::commit();
     return response()->json([
       'status'=>'success',
       'result'=>$trans,
     ]);
   }

   public function api_detail($id)
   {
         $trans= Transaksi::with('pembeli','jenisPembayaran','detailTransaksi.stokBarang.barangJual.barang')->find($id);
         return response()->json([
           'status'=>'success',
           'result'=>$trans
         ]);
   }

   public function api_riwayat()
   {
         $riwayat = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                              ->where('users.id', Auth::user()->id)
                              // ->where('transaksi.status', 'Success')
                              // ->orWhere('transaksi.status', 'Failed')
                              ->select('transaksi.*')
                              ->get();
         return response()->json([
           'status'=>'success',
           'result'=>$riwayat
         ]);
   }

   public function api_pilihMetode()
   {
     $data = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                         ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                         ->where('users.id', Auth::user()->id)
                         ->whereNull('transaksi.jenis_pembayaran_id')
                         ->select('transaksi.*')
                         ->get();
     if (empty($data)) {
       return response()->json([
         'status'=>'success',
         'result'=>'empty'
       ]);
     }
     return response()->json([
       'status'=>'success',
       'result'=>$data
     ]);
   }

   public function api_waiting()
   {
     $data = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                          ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                          ->where('users.id', Auth::user()->id)
                          ->where('transaksi.status', 'Waiting')
                          ->whereNotNull('transaksi.jenis_pembayaran_id')
                          ->select('transaksi.*')
                          ->get();
    // dd($data);
    if (empty($data)) {
      return response()->json([
        'status'=>'success',
        'result'=>'empty'
      ]);
    }
     return response()->json([
       'status'=>'success',
       'result'=>$data
     ]);
   }
}
