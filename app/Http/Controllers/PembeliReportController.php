<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pembeli;
use App\Status;
use App\DetailTransaksiKonven;
use Carbon\Carbon;
use PDF;
use App\Exports\PembeliExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
Use Alert;

class PembeliReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pembeli = User::where('level','Pembeli')->get()->sortBy('name');
      return view('pembeli-report.find',compact('pembeli'));
      // dd($pembeli);
    }

    public function laporan(Request $request)
    {
      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Date not match!');
      }
      $this->validate(request(),
        [
          'pembeli' => 'required',
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'pembeli.required' => 'Buyer can not be empty!',
          'from.required' => 'Date from can not be empty!',
          'until.required' => 'Date until can not be empty!',
        ]
      );
      // dd($request->from = Carbon::parse($request->from));
      // dd($request->until = Carbon::parse($request->until)->addDay(1));
      $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                              ->where('users.id', $request->pembeli)
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama', 'barang_konven.harga', 'penjual_konven.nama_toko', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

      $pembeli = User::where('id',$request->pembeli)->first();
      $pembeliAll = User::where('level','Pembeli')->get()->sortBy('name');
      $requested = $request;
      
      return view('pembeli-report.laporan',compact('pembeli','list','requested', 'pembeliAll', 'outcomeTotal', 'itemTotal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function downloadPDF(request $request){
       $list = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                               ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                               ->leftJoin('jenis_pembayaran', 'transaksi.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                               ->leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                               ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                               ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                               ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                               // ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                               ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                               ->where('users.id', $request->pembeli)
                               ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                               ->where('transaksi.updated_at', '<=', $request->until = Carbon::parse($request->until))
                               ->select('transaksi.updated_at', 'barang.nama', 'stok_barang.harga', 'detail_transaksi.kuantitas', 'jenis_pembayaran.jenis_pembayaran', 'lokasi.nama as lokasi', 'transaksi.status', 'transaksi.id as transaksi_id')
                               ->get();
       $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                       ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                       ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                       ->leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                       ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                       ->where('transaksi.status','=','success')
                       ->where('pembeli.user_id', $request->pembeli)
                       ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                       ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                       ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as outcome, sum(kuantitas) as item')
                       ->get();
                       // dd($solds);
       $outcomeTotal=0;
       $itemTotal=0;
       foreach ($solds as $key => $value) {
           $outcomeTotal += $value['outcome'];
           $itemTotal += $value['item'];
       }
       $pembeli = User::where('id',$request->pembeli)->first();
      //  dd($pembeli);
       // dd($collection);
      //  dd($request);
      //  return view('pembeli-report.pdf',compact('list', 'request', 'pembeli', 'outcomeTotal', 'itemTotal'));
       $pdf = PDF::loadView('pembeli-report.pdf', compact('list', 'request', 'pembeli', 'outcomeTotal', 'itemTotal'));
       $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
       $request->from = Carbon::parse($request->from)->format('dmY');
       return $pdf->download('report-'.$pembeli->name.'-'.$request->from.'-'.$request->until.'.pdf');

     }
     public function downloadExcel(Request $request)
     {
       // dd($request);
       $list = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
       ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
       ->leftJoin('jenis_pembayaran', 'transaksi.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
       ->leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
       ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
       ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
       ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
       ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
       ->where('users.id', $request->pembeli)
       ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
       ->where('transaksi.updated_at', '<=', $request->until = Carbon::parse($request->until))
       ->select('transaksi.updated_at', 'transaksi.status', 'transaksi.id as transaksi_id', 'barang.nama', 'lokasi.nama as lokasi', 'stok_barang.harga', 'detail_transaksi.kuantitas', 'jenis_pembayaran.jenis_pembayaran')
       ->get();
       $pembeli = User::where('id',$request->pembeli)->first();
       $collection = [];
       foreach ($list as $i => $a) {
         $collection[$i] = [
           'transaksi_id' => $a['transaksi_id'],
           'updated_at' => Carbon::parse($a['updated_at'])->format('l, j F Y - H:i'),
           'nama' => $a['nama'],
           'lokasi' => $a['lokasi'],
           'harga' => $a['harga'],
           'kuantitas' => $a['kuantitas'],
           'income' => $a['harga'] * $a['kuantitas'],
           'jenis_pembayaran' => $a['jenis_pembayaran'],
           'status' => $a['status'],
         ];
       }
       // dd($collection, $list);
       $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
       $request->from = Carbon::parse($request->from)->format('dmY');

       $exporter = app()->makeWith(PembeliExport::class, compact('collection'));
       return $exporter->download('report-'.$pembeli->name.'-'.$request->from.'-'.$request->until.'.xlsx');
     }
}
