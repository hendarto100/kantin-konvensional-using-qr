<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function penjual()
    {
        return view('report.penjual');
    }
    public function pembeli()
    {
        return view('report.pembeli');
    }
    public function waktu()
    {
        return view('report.waktu');
    }
}
