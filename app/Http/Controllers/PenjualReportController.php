<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Barang;
use App\PenjualKonven;
use App\BarangJual;
use App\StokBarang;
use App\DetailTransaksiKonven;
use App\TransaksiKonven;
use App\StatusHarian;
use App\Status;
use App\LogStok;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use PDF;
use App\Exports\PenjualExport;
use App\Exports\PenjualHarianExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
Use Alert;
use DB;


class PenjualReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $penjual = User::where('level','Kasir')->get()->sortBy('name');
        // $penjual = Penjual::all()->sortBy('name');
        return view('penjual-report.find',compact('penjual'));
        // dd($pembeli);
    }

    public function detailStok($id)
    {
        $data = LogStok::where('stok_barang_id',$id)->get();
        return view ('penjual-report.detail-stok', compact('data'));
    }

    public function laporan(Request $request)
    {
      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Date not match!');
      }
      $this->validate(request(),
        [
          'penjual' => 'required',
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'penjual.required' => 'Seller can not be empty!',
          'from.required' => 'Date from can not be empty!',
          'until.required' => 'Date until can not be empty!',
        ]
      );
      $requested = $request;
      $penjualAll = User::where('level','Kasir')->get()->sortBy('name');
      // dd($request);
      //ONLY ONE DAY
       $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                              ->where('users.id', $request->penjual)
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama', 'barang_konven.harga', 'penjual_konven.nama_toko', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

        $penjual = User::where('id',$request->penjual)->first();
        // dd($requested);
        return view('penjual-report.laporan',compact('penjual','list', 'requested', 'penjualAll', 'incomeTotal', 'itemTotal'));
      }

    public function downloadPDF(request $request){

        //ONLY ONE DAY
        if (Carbon::parse($request->from) == Carbon::parse($request->until)->addDay(-1)) {
          $list = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                                  ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                                  ->leftJoin('jenis_pembayaran', 'transaksi.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                                  ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                                  ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                                  ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                                  ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                                  ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                                  ->where('users.id', $request->penjual)
                                  ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                                  ->where('transaksi.updated_at', '<=', $request->until = Carbon::parse($request->until))
                                  ->where('transaksi.status', 'success')
                                  ->select('transaksi.updated_at', 'barang.nama', 'stok_barang.harga', 'detail_transaksi.kuantitas', 'jenis_pembayaran.jenis_pembayaran', 'transaksi.id as transaksi_id', 'lokasi.nama as lokasi')
                                  ->get();
          $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                          ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                          ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                          ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                          ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                          ->where('transaksi.status','=','success')
                          ->where('penjual.user_id', $request->penjual)
                          ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                          ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                          ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                          ->get();
                          // dd($total);
          $incomeTotal=0;
          $itemTotal=0;
          foreach ($solds as $key => $value) {
              $incomeTotal += $value['income'];
              $itemTotal += $value['item'];
          }
          $penjual = User::where('id',$request->penjual)->first();
          // return view('penjual-report.pdf',compact('penjual','list', 'request', 'incomeTotal', 'itemTotal'));
          $pdf = PDF::loadView('penjual-report.pdf', compact('penjual','list', 'request', 'incomeTotal', 'itemTotal'));
          $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
          $request->from = Carbon::parse($request->from)->format('dmY');
          return $pdf->download('report-'.$penjual->name.'-'.$request->from.'-'.$request->until.'.pdf');
        }
        //WITH RANGE DAY
        else {
          $list = StokBarang::leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                                  ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                                  ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                                  ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                                  ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                                  ->where('users.id', $request->penjual)
                                  ->where('stok_barang.created_at','>=', $request->from = Carbon::parse($request->from))
                                  ->where('stok_barang.created_at','<=', $request->until = Carbon::parse($request->until))
                                  ->select('stok_barang.created_at', 'stok_barang.id', 'barang.nama', 'stok_barang.harga', 'stok_barang.jumlah', 'lokasi.nama as lokasi')
                                  ->get()
                                  ->toArray();
          // dd($request);

          $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                          ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                          ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                          ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                          ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                          ->where('transaksi.status','=','success')
                          ->where('penjual.user_id', $request->penjual)
                          ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                          ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                          ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                          ->get();
          $incomeTotal=0;
          $itemTotal=0;
          foreach ($solds as $key => $value) {
              $incomeTotal += $value['income'];
              $itemTotal += $value['item'];
          }
          $penjual = User::where('id',$request->penjual)->first();

          $collection = [];
          foreach ($list as $i => $stok) {
            $collection[$i] = [
              'id' => $stok['id'],
              'nama' => $stok['nama'],
              'harga' => $stok['harga'],
              'lokasi' => $stok['lokasi'],
              'jumlah' => $stok['jumlah'],
              'sold'  => 0,
              'remain' => $stok['jumlah'],
              'income' => 0,
              'created_at' => $stok['created_at'] = Carbon::parse($stok['created_at'])->format('l, j F Y'),
              ];
            foreach ($solds as $j => $sold) {
              if ($sold->stok_barang_id == $stok['id']) {
                $collection[$i]['sold']  = $sold->item;
                $collection[$i]['remain']  = $stok['jumlah'] - $sold->item;
                $collection[$i]['income']  = $sold->item * $stok['harga'];
              }
            }
          }
      // dd($request);
      // dd($collection);
      // return view('barang-report.pdf',compact('barang','list','collection', 'request'));
      // return view('penjual-report.pdf',compact('penjual','list','collection', 'request', 'incomeTotal', 'itemTotal'));
      $pdf = PDF::loadView('penjual-report.pdf', compact('penjual','list','collection', 'request', 'incomeTotal', 'itemTotal'));
      $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
      $request->from = Carbon::parse($request->from)->format('dmY');
      return $pdf->download('report-'.$penjual->name.'-'.$request->from.'-'.$request->until.'.pdf');
    }
  }

  public function downloadExcel(Request $request)
  {
    //ONLY ONE DAY
    if (Carbon::parse($request->from) == Carbon::parse($request->until)->addDay(-1)) {
      $list = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                              ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                              ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                              ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                              ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                              ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                              ->where('users.id', $request->penjual)
                              ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi.updated_at', '<=', $request->until = Carbon::parse($request->until))
                              ->where('transaksi.status', 'success')
                              ->select('transaksi.updated_at', 'barang.nama', 'stok_barang.harga', 'detail_transaksi.kuantitas', 'jenis_pembayaran.jenis_pembayaran', 'transaksi.id as transaksi_id', 'lokasi.nama as lokasi')
                              ->get()->toArray();
      $penjual = User::where('id',$request->penjual)->first();
      $collection = [];
      foreach ($list as $i => $stok) {
        $collection[$i] = [
          'transaksi' => $stok['transaksi_id'],
          'updated_at' => $stok['updated_at'] = Carbon::parse($stok['updated_at'])->format('l, j F Y'),
          // 'id' => $stok['id'],
          'nama' => $stok['nama'],
          'lokasi' => $stok['lokasi'],
          'harga' => $stok['harga'],
          'jumlah' => $stok['kuantitas'],
          'payment' => $stok['jenis_pembayaran'],
          'income' => $stok['harga'] * $stok['kuantitas'],
          ];
      }
      // dd($collection);

      // $requested = $request;
      // dd($requested);
      // return view('penjual-report.laporan-harian',compact('penjual','list', 'requested'));
      // dd($request);
      // return view('penjual-report.pdf',compact('penjual','list', 'request'));
      $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
      $request->from = Carbon::parse($request->from)->format('dmY');

      $exporter = app()->makeWith(PenjualHarianExport::class, compact('collection'));
      return $exporter->download('report-'.$penjual->name.'-'.$request->from.'-'.$request->until.'.xlsx');
    }
    //WITH RANGE DAY
    else {
      $list = StokBarang::leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                              ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                              ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                              ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                              ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                              ->where('users.id', $request->penjual)
                              ->where('stok_barang.created_at','>=', $request->from = Carbon::parse($request->from))
                              ->where('stok_barang.created_at','<=', $request->until = Carbon::parse($request->until))
                              ->select('stok_barang.created_at', 'stok_barang.id', 'barang.nama', 'stok_barang.harga', 'stok_barang.jumlah', 'lokasi.nama as lokasi')
                              ->get()
                              ->toArray();
      // dd($request);

      $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                      ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                      ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                      ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                      ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                      ->where('transaksi.status','=','success')
                      ->where('penjual.user_id', $request->penjual)
                      ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                      ->where('transaksi.updated_at', '<=', $request->until = Carbon::parse($request->until))
                      ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                      ->get();

      $penjual = User::where('id',$request->penjual)->first();

      $collection = [];
      foreach ($list as $i => $stok) {
        $collection[$i] = [
          // 'id' => $stok['id'],
          'created_at' => $stok['created_at'] = Carbon::parse($stok['created_at'])->format('l, j F Y'),
          'lokasi' => $stok['lokasi'],
          'nama' => $stok['nama'],
          'harga' => $stok['harga'],
          'jumlah' => $stok['jumlah'],
          'sold'  => 0,
          'remain' => $stok['jumlah'],
          'income' => 0,
          ];
        foreach ($solds as $j => $sold) {
          if ($sold->stok_barang_id == $stok['id']) {
            $collection[$i]['sold']  = $sold->item;
            $collection[$i]['remain']  = $stok['jumlah'] - $sold->item;
            $collection[$i]['income']  = $sold->item * $stok['harga'];
          }
        }
      }
    $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
    $request->from = Carbon::parse($request->from)->format('dmY');
    $exporter = app()->makeWith(PenjualExport::class, compact('collection'));
    return $exporter->download('report-'.$penjual->name.'-'.$request->from.'-'.$request->until.'.xlsx');
    }
  }
}
