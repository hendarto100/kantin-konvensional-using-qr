<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Bank extends Model
{
  use SoftDeletes;
  protected $table = 'bank';
  // protected $fillable = ['nama','deskripsi'];
  public $timestamps = true;
  protected $guarded = ['created_at', 'updated_at', 'deleted_at'];
  // protected $dates = [];

  public function penjual()
  {
    return $this->hasMany('App\Bank', 'bank_id', 'id');
  }
}
