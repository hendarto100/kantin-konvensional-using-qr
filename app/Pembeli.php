<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pembeli extends Model
{
  use SoftDeletes;
  protected $table = 'pembeli';
  protected $guarded = ['created_at', 'updated_at', 'deleted_at'];
  // protected $fillable = ['user_id', 'saldo'];
  public $timestamps = true;

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  public function user()
      {
          return $this->belongsTo('App\User', 'user_id', 'id');
      }
}
