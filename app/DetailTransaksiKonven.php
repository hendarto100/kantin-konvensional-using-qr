<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksiKonven extends Model
{
    protected $table = 'detail_transaksi_konven';
    public $primaryKey='transaksi_konven_id';
  	protected $fillable = ['barang_konven_id','transaksi_konven_id','harga','diskon','jumlah','total'];
  	public $timestamps = true;

  public function transaksiKonven()
  {
    return $this->belongsTo('App\TransaksiKonven', 'transaksi_konven_id', 'id');
  }
  public function logSaldo()
  {
      return $this->morphOne('App\LogSaldo', 'topup', 'source', 'reference_id');
  }

  public function barang_konven()
  {
      return $this->belongsTo('App\BarangKonven', 'barang_konven_id','id');
  }
}
