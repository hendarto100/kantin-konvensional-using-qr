<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['created_at', 'updated_at', 'deleted_at'];
    // protected $fillable = [
    //     'name', 'email', 'password', 'tanggal_lahir','level','no_telepon', 'status_id', 'foto',
    // ];
    // protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function penjual_konven()
    {
        return $this->hasMany('App\PenjualKonven', 'user_id','id');
    }
    public function pembeli()
    {
        return $this->hasOne('App\Pembeli', 'user_id', 'id');
    }

    public function pencairan()
    {
        return $this->hasMany('App\Pencairan', 'user_id', 'id');
    }
    public function topup()
    {
        return $this->hasMany('App\Topup', 'user_id', 'id');
    }
    public function logSaldo()
    {
        return $this->hasMany('App\LogSaldo', 'user_id', 'id');
    }
///////////////////////////////

    public function tes()
    {
        return $this->hasMany('App\PenjualKonven', 'id','user_id');
    }
   
    public function BarangKonven()
    {
        return $this->hasManyThrough('App\BarangKonven', 'App\PenjualKonven');
    }

}
