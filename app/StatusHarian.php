<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusHarian extends Model
{
    protected $table='status_harian';
    protected $fillable=['status_harian'];
    public $timestamps=true;
}
