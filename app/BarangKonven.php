<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class BarangKonven extends Model
{
  	use softDeletes;
    protected $table = 'barang_konven';
    // protected $fillable = ['penjual_id', 'barang_id', 'harga', 'lokasi_id'];
    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
    protected $primaryKey = 'id';
    public $incrementing = false;
    // protected $dates = ['deleted_at'];

    
    public function penjual_konven()
    {
      return $this->belongsTo('App\PenjualKonven', 'penjual_konven_id', 'id');
    }

    public function detail_transaksi_konven()
    {
        return $this->hasMany('App\DetailTransaksiKonven', 'barang_konven_id', 'id');
    }

    public function keranjang()
      {
        return $this->hasMany('App\Keranjang', 'barang_konven_id'. 'id');
      }
}
