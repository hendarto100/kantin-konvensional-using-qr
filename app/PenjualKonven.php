<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PenjualKonven extends Model
{
  protected $table = 'penjual_konven';
  protected $fillable = ['saldo','nama_toko'];
  public $timestamps = true;
  protected $primaryKey = 'id';

  public function user()
      {
          return $this->belongsTo('App\User', 'user_id', 'id');
      }
   public function barangKonven()
	  {
	    return $this->hasMany('App\BarangKonven', 'barangl_konven_id', 'id');
	  }
    public function transaksi_konven()
      {
        return $this->hasMany('App\TransaksiKonven', 'penjual_konven_id'. 'id');
      }

    public function keranjang()
      {
        return $this->hasMany('App\Keranjang', 'penjual_konven_id'. 'id');
      }

}
