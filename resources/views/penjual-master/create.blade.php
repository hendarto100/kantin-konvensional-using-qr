@extends('master')

@section('css')
  <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="#">User</a> /
  <a href="{{url('/master/penjual')}}">Seller</a> /
  <a href="{{url('#')}}">Add</a>
@stop

@section('title')
  <h3>User</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Tambah Penjual</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(array('url'=>'master/penjual_konven/store', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'files'=>'true','enctype'=>'multipart/form-data', 'novalidate'))!!}
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Foto <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
                {{-- <input class="form-control" placeholder="photo" name="photo" value="{{ old('name') }}" type="file"  class="form-control col-md-7 col-xs-12"> --}}
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="" name="name" value="{{ old('name') }}" type="text"  class="form-control col-md-7 col-xs-12">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="" name="email" value="{{ old('email') }}" type="email"  class="form-control col-md-7 col-xs-12">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nomer HP <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="" name="no_telepon" value="{{ old('no_telepon') }}" type="number"  class="form-control col-md-7 col-xs-12">
                @if ($errors->has('no_telepon'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('no_telepon') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Password <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" placeholder="" name="password" value="{{ old('password') }}" type="password" type="text" >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Lahir <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                    <input name="tanggal_lahir" type="text" name="until" class="form-control datepicker" placeholder="MM/DD/YYYY" value="{{old('tanggal_lahir')}}" >
                </div>
                @if ($errors->has('tanggal_lahir'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="saldo">Saldo <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="" name="saldo" value="{{ old('saldo') }}" type="number"  class="form-control col-md-7 col-xs-12">
                @if ($errors->has('saldo'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('saldo') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">No Rekening
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name="no_rekening" type="number" class="form-control" placeholder="" value="{{old('no_rekening')}}" >
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/penjual_konven')}}'">Cancel</a>
                <button id="send" type="submit" class="btn btn-success">Save</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
            $('.select2').select2();

            // Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'yy-mm-dd'
            });

        });
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
        // format: "dd/mm/yyyy",
        endDate: "dd",
        autoclose: true,
        todayHighlight: true,
      });
     });
    </script>

@endsection
