@extends('layout')

@section('css')
<link href="{{asset('lib/highlightjs/github.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('breadcrumbs')
<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
    <a class="breadcrumb-item" href="#">Report</a>
    <a class="breadcrumb-item" href="{{url('report/pembeli')}}">Pembeli</a>
</nav>
@stop

@section('title')
<div class="sl-page-title">
    <h5>Laporan Berdasar Pembeli</h5>
</div><!-- sl-page-title -->
@stop

@section('content')
	<div class="card pd-10 pd-sm-20">
    <div>
      <div class="row mg-t-10" style="margin-top: 5px;">
          <div class="col-md-4">
              <p>Nama Pembeli</p>
          </div>
          <div class="col-md-8">
            <input class="form-control" placeholder="nama pembeli" name="nama" value="{{ old('nama') }}" type="text">
          </div>
      </div>
      <div class="row mg-t-10" style="margin-top: 5px;">
          <div class="col-md-4">
              <p>Tanggal</p>
          </div>
          <div class="col-md-3">
            <input class="form-control" name="tanggal_awal" value="{{ old('tanggal_awal') }}" type="date">
          </div>
          <div class="col-md-2" style="margin-top: 10px;">
            <p style="margin-left: 65px;">s/d</p>
          </div>
          <div class="col-md-3">
            <input class="form-control" name="tanggal_akhir" value="{{ old('tanggal_akhir') }}" type="date">
          </div>
      </div>
      <div class="col-sm-6 col-md-3" style="margin-top: 5px;">
          <a href="{{url('#')}}" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i>GO!</a>
      </div><!-- col-sm -->
    </div>
  </div>
  <div class="card pd-10 pd-sm-20" style="margin-top : 20px">
    <div style="margin-top: 5px;">
        <div class="col-md-4">
            <p> <a href="{{url('#')}}"><i class="fa fa-save mg-r-10"></i>Save as to PDF</a></p>
            <a>Pembeli : Afyad Kafa</a><br>
            <a>Tanggal : 12 Oktober 2018 - 15 Oktober 2018</a>
        </div>
    </div>
    <div class="table-wrapper mg-t-10">
        <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>Barang</th>
                <th>Harga</th>
                <th>Kuantitas</th>
                <th>Pembayaran</th>
                <th>Jenis Pembayaran</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
