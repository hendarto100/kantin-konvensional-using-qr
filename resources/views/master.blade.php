<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <link rel="icon" href="{{ asset('image/logo-ugm.png')}}" type="image/ico" /> --}}

    <title>Kantin UGM</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('assets/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{asset('assets/build/css/custom.min.css')}}" rel="stylesheet">

    @yield('css')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col {{--menu_fixed--}}">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('/')}}" class="site_title"><i class="fa fa-paw"></i> <span>Kantin UGM</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <a href="{{url('profile/'.Auth::user()->id.'')}}" >
                  @if (empty(Auth::user()->foto))
                    <img src="{{asset('images/avatar.png')}}" alt="..." class="img-circle profile_img">
                  @else
                    <img class="img-circle profile_img" src="{{asset('images/'.Auth::user()->foto.'')}}" alt="Avatar" title="Change the avatar">
                  @endif
                </a>
              </div>
              <div class="profile_info">
                <span>Hello!</span>
                <a href="{{url('profile/'.Auth::user()->id.'')}}" >
                  <h2>{{Auth::user()->name}}</h2>
                </a>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                  <li><a href="{{url('/transaksi_admin')}}"><i class="fa fa-money"></i> Transaksi</a></li>
                  <li><a><i class="fa fa-sort-amount-desc"></i> Kelola Gamapay<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/pencairan')}}">Pencairan</a></li>
                      <li><a href="{{url('/topup')}}">Top Up</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Laporan<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('laporan/pembeli')}}">Berdasarkan Pembeli</a></li>
                      <!-- <li><a href="{{url('laporan/barang')}}">Berdasarkan Produk</a></li> -->
                      <li><a href="{{url('laporan/penjual')}}">Berdasarkan Penjual</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> Pengguna<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('master/pembeli')}}">Member</a></li>
                      <li><a href="{{url('master/pembeli_bukan_member')}}">Pembeli Bukan Member</a></li>
                      <li><a href="{{url('master/penjual_konven')}}">Penjual</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-database"></i> Data Master <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('master/bank')}}">Bank</a></li>
                      <!-- {{-- <li><a href="{{url('master/status-harian')}}">Daily Status</a></li> --}} -->
                      <li><a href="{{url('master/kategori')}}">Kategori Produk</a></li>
                      <li><a href="{{url('master/barang_konven')}}">Produk</a></li>
                      <!-- <li><a href="{{url('master/lokasi')}}">Location</a></li> -->
                      <li><a href="{{url('master/jenis-pembayaran')}}">Metode Pembayaran</a></li>
                      <!-- <li><a href="{{url('master/status')}}">Status</a></li> -->
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            {{-- <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> --}}
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('images/'.Auth::user()->foto.'')}}" alt="ava">{{Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{url('profile/'.Auth::user()->id.'')}}"> Profile</a></li>
                    {{-- <li>
                      <a href="javascript:;">
                        <span>Settings</span>
                      </a>
                    </li> --}}
                    <li>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>

                      {{-- <a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a> --}}
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                @yield('navigation')
                {{-- <a>Some examples to get you started</a> --}}
                @yield('title')
                {{-- <h3>Users </h3> --}}
              </div>
            </div>
            <div class="clearfix">
            </div>
            {{-- @if (session('alert'))
              <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-body text-center alert alert-success">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <i class="fa fa-4x fa-check-circle-o"></i>
                      <h5>{{ session('alert') }}</h5>
                    </div>
                </div>
              </div>
            @endif --}}
            @yield('content')
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->

        <footer>
          <div class="pull-right">
            <a>UGM Pay Canteen - Love You Mom :* (mwah)</a>
            {{-- Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a> --}}
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('assets/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('assets/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- validator -->
    <script src="{{asset('assets/vendors/validator/validator.js')}}"></script>
    <!-- <script src="{{asset('vendor/sweetalert/sweetalert.all.js')}}"></script> -->
    <script src="{{asset('assets/vendors/inputmask/jquery.inputmask.bundle.js')}}"></script>
    <script type="text/javascript">
    function numberWithCommas(x) {
      var bil =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      return "Rp " + bil;
    }
        $(document).ready(function(){
          $('.currency').inputmask("numeric", {
                      radixPoint: ",",
                      groupSeparator: ".",
                      digits: 2,
                      autoGroup: true,
                      rightAlign: false,
                      removeMaskOnSubmit: true,
                      oncleared: function () { self.Value(''); }
                  });
        });
    </script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('assets/build/js/custom.min.js')}}"></script>
    {{-- <script>$("#myModal").modal("show");</script> --}}
    @yield('javascript')
    

  </body>
</html>
