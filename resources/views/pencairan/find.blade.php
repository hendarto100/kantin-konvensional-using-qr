@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Gamapay</a> /
  <a href="{{url('#')}}">Top Up</a>
@stop

@section('title')
  <h3>Pay-Jur</h3>
@stop

@section('content')
  @if (count($errors)>0)
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                    @foreach($errors->all() as $error)
                      <a>{{$error}}<br></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Cari Pembeli<small></small></h2>
          <div class="clearfix">
          </div>
        </div>
        {!! Form::open(array('url'=>'/pencairan/detail-user', 'method'=>'GET', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}
        <div class="x_content">
          <table id="datatable1" class="table table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Username</th>
                <th>Email</th>
                <th>No Telepon</th>
                <th class="col-md-1">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $pembeli)
              <tr class="item-{{$pembeli->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$pembeli->name}}</td>
                  <td>{{$pembeli->email}}</td>
                  <td>{{$pembeli->no_telepon}}</td>
                  <td>
                      <a href="{{url('pencairan/detail-user/'.$pembeli->id.'')}}" class="btn btn-success btn-icon btn-xs rounded-circle mg-r-5 mg-b-10" title="Top Up">
                          <div><i class="fa fa-min"></i> Ambil Uang</div>
                      </a>
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
  @if (session('error'))
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                      <a>{{ session('error') }}</a>
                </div>
            </div>
        </div>
    </div>
  @endif
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
               // scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
    <script>$("#myModalError").modal("show");</script>
    <script>$("#myError").modal("show");</script>


@endsection
