@extends('master')

@section('css')

@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/kategori')}}">Kategori</a> /
  <a href="{{url('/master/kategori/add')}}">Tambah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Tambah Kategori</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          {{-- <form class="form-horizontal form-label-left" novalidate> --}}
            {!! Form::open(array('url'=>'master/kategori/store', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}

            {{-- <span class="section">Category Info</span> --}}
            @if(count($errors)>0)
              <div class="alert alert-warning">
                @foreach($errors->all() as $error)
                  <li>
                    {{$error}}
                  </li>
                @endforeach
              </div>
            @endif
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="kategori" name="nama" value="{{ old('nama') }}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="textarea" placeholder="tulis deskripsi" name="deskripsi"  type="text" class="form-control col-md-7 col-xs-12">{{ old('deskripsi') }}</textarea>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/kategori')}}'">Batal</a>
                <button id="send" type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
