@section('js')

<!-- untuk ajax yang ambil data -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- untuk auto complete -->
<link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
<script src="http://demo.expertphp.in/js/jquery.js"></script>
<script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>

<script type="text/javascript">

  $('#nama_pembeli').keyup(function(){ 
         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('autocomplete.fetch') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            $('#listPembeli').fadeIn();  
                     $('#listPembeli').html(data);
           }
          });
         }
     });

     $('#listPembeli').on('click', 'li', function(){  
         $('#nama_pembeli').val($(this).text());  
         $('#listPembeli').fadeOut();

         var idfromfield = $('#nama_pembeli').val();
         var url= "{{url('transaksi_konven')}}/"+idfromfield+"/getNamaPembeli"
         $.get(url).done(function(result){
           $('#id_pembeli').val(result.id)
           $('#saldo_pembeli').val(result.saldo)

         });
        
     });  

</script>
 
<script type="text/javascript">
  $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

<script type="text/javascript">


    $('#id_produk').keyup(function(){
                var idfromfield = $('#id_produk').val();
                var url= "{{url('transaksi_konven')}}/"+idfromfield+"/getData"
                $.get(url).done(function(result){
                  $('#harga').val(result.harga)
                  $('#id_produk').val(result.id)
                  $('#nama').val(result.nama)
                  $('#stok').val(result.stok)

                })
              });

   

                var total = 0;
                $('#jumlah').keyup(function(){
                  var jumlahBarang = $('#jumlah').val();
                  var hargaBarang = $('#harga').val();
                  var subtotal = jumlahBarang*hargaBarang;

                  $('#subtotal').val(subtotal);
                });
                  $('#total').val(total);

</script>

<script type="text/javascript">

    src = "{{ route('searchajax') }}";
     $("#nama").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 2,
       
    });


    $('#nama').keyup(function(){
                var namaProduk = $('#nama').val();
                var url= "{{url('transaksi_konven')}}/"+namaProduk+"/getDataProduk"
                $.get(url).done(function(result){
                  $('#harga').val(result.harga)
                  $('#id_produk').val(result.id)
                  $('#potongan').val(result.diskon)
                  $('#stok').val(result.stok)

                })
              });

    $('#jumlah').click(function(){
                var namaProduk = $('#nama').val();
                var url= "{{url('transaksi_konven')}}/"+namaProduk+"/getDataProduk"
                $.get(url).done(function(result){
                  $('#harga').val(result.harga)
                  $('#id_produk').val(result.id)
                  $('#potongan').val(result.diskon)
                  $('#stok').val(result.stok)

                })
              });

                var total = 0;
                var diskon = 0;
                $('#jumlah').keyup(function(){
                  var jumlahBarang = $('#jumlah').val();
                  var hargaBarang = $('#harga').val();
                  var potongan = $('#potongan').val();
                  var totalPotongan = jumlahBarang*potongan;
                  var subtotal = jumlahBarang*(hargaBarang-potongan);

                  $('#subtotal').val(subtotal);
                });
                  $('#total').val(total);
                  $('#diskon').val(diskon);

</script>

<script type="text/javascript">
$(document).ready(function(){
  
        $("#tambah").click(function(){
            var id_produk = $("#id_produk").val();
            var nama = $("#nama").val();
            var harga = $("#harga").val();
            var jumlah = $("#jumlah").val();
            var potongan = $("#potongan").val();
            var totalPotongan = jumlah*potongan;
            var stok = $("#stok").val();
            var subtotal = $("#subtotal").val();

            if (nama == '') {
              $("#nama").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (jumlah == '') {
              $("#jumlah").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (id_produk == '') {
              $("#id_produk").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (harga == '') {
              $("#harga").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (potongan == '') {
              $("#potongan").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (subtotal == '') {
              $("#subtotal").after("<span style='color:red;' class='span_jumlah'>Harus diisi</span>");
            }
            else if (parseInt(stok) < parseInt(jumlah)) {
              $("#stok").after("<span style='color:red;' class='span_jumlah'>Stok kurang</span>");
            }
            //     // $("#id_barang").val('');
            //     // $("#nama_barang").val('');
            //     // $("#jml_stok").val('');
            //     // $("#harga").val('');
            //     $("#jumlah").val('');
            //     $("#sub_total").val('');                
            // }
            else{

              $(".span_jumlah ").remove();
            
              var markup = "<tr><td><input type='hidden' value='" + id_produk + "' name='id_produk[]'>"+id_produk+"</td><td><input type='hidden' value='" + nama + "' name='nama[]'>"+nama+"</td><td><input type='hidden' value='" + harga + "' name='harga[]'>"+harga+"</td><td><input type='hidden' class='potongan' value='"+potongan+"' name='potongan[]'>"+potongan+"</td><td><input type='hidden' value='" + jumlah + "' name='jumlah[]'>"+jumlah+"</td><td><input type='hidden' class='sub_total' value='" + subtotal + "' name='subtotal[]'>"+subtotal+"</td><td><input type='button' value='Hapus' id='hapus' class='btn btn-danger' name='record'></td></tr>";
              
              $("#daftar").append(markup);
              var totalHarga = $("#total").val();
              var totalDiskon = $("#diskon").val();
              
              if(totalHarga==null){
                totalHarga=0;
                totalDiskon=0;
              }
                totalPotongan=parseInt(totalDiskon)+parseInt(totalPotongan);
                totalHarga=parseInt(totalHarga)+parseInt(subtotal);

              $("#total").val(totalHarga);
              $("#diskon").val(totalPotongan);


              var awal = '';
              $("#nama").val(awal);
              $("#id_produk").val(awal);
              $("#harga").val(awal);
              $("#stok").val(awal);
              $("#subtotal").val(awal);
              $("#jumlah").val(awal);
              $("#potongan").val(awal);
          }
            
        });
      
      $(document).on("click",'.btn-danger', function(){
          $(this).closest('tr').remove();
          var subtotal = $(this).closest('tr').find('.sub_total').val();
          var potongan = $(this).closest('tr').find('.potongan').val();
          var totalHarga = $("#total").val();
              totalHarga=totalHarga-subtotal;
          var totalPotongan = $("#diskon").val();
              totalPotongan=totalPotongan-potongan;

            $('#total').val(totalHarga);
            $('#diskon').val(totalPotongan);
            $(this).closest('tr').remove();
      });

      $(document).on('keyup','#bayar',function(e){
        
          var bayar = $('#bayar').val();
          var total = $('#total').val();
          var value = parseInt(bayar)-parseInt(total);
          $("#kembali").val(value);
      });
}); 

</script>

<script type="text/javascript">


    $('#nama_pembeli').keyup(function(){
                var idfromfield = $('#nama_pembeli').val();
                var url= "{{url('transaksi_konven')}}/"+idfromfield+"/getNamaPembeli"
                $.get(url).done(function(result){
                  $('#id_pembeli').val(result.id)
                  $('#saldo_pembeli').val(result.saldo)

                })
              });

</script>


<script type="text/javascript">
  $(document).ready(function(){

   $('#id_pembeli').keyup(function(){
                var idPembeli = $('#id_pembeli').val();
                var url= "{{url('transaksi_konven')}}/"+idPembeli+"/getDataPembeli"
                $.get(url).done(function(result){
                  $('#nama_pembeli').val(result.nama)
                  $('#saldo_pembeli').val(result.saldo)

                })
              });
  });
</script>

<script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi Baru
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Transaksi</a></li>
        <li class="active">Transaksi Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>      

      <div class="row">       

        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Input Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">

                      <div class="col-md-2">
                        <label for="nama">Nama Produk</label>
                        <input class="form-control" id="nama" name="nama" placeholder="Masukan nama" type="text">
                        <div id="namaList">
                        </div>
                      </div>
                      <div class="col-md-1">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" id="jumlah" name="jumlah" placeholder="0" type="text">
                      </div>
                      <div class="col-md-1">
                        <label for="id">Id</label>
                        <input class="form-control" id="id_produk" name="id_produk" placeholder="Id" type="text" readonly="true">
                      </div>
                      <div class="col-md-1">
                        <label for="stok">Stok</label>
                        <input class="form-control" id="stok" name="stok" placeholder="0" type="text" readonly="true">
                      </div>
                      <div class="col-md-2">
                        <label for="Harga">Harga</label>
                        <input class="form-control" id="harga" name="harga" placeholder="0" type="text" readonly="true">
                      </div>

                      <div class="col-md-1">
                        <label for="id">Diskon</label>
                        <input class="form-control" id="potongan" name="potongan" placeholder="0" type="text" readonly="true">
                      </div>
    
                      <div class="col-md-2">
                        <label for="subtotal">Subtotal</label>
                        <input class="form-control" id="subtotal" name="subtotal" placeholder="0" type="text" readonly="true">                        
                      </div> 
                      <div class="col-md-2">
                        <label for="subtotal">Tambahkan</label>
                        <input type="button" name="tambah" id="tambah" value="Tambah produk" class="btn btn-primary">
                      </div>                    
                </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="left">

              <p style="color:blue">Produk yang tersedia adalah produk yang sudah anda inputkan di toko milik Anda.</p>
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        
        
      </div>
      <!-- /.row -->

            
    <div class="row">

      <div class="col-md-7">
        <form class="form-horizontal" method="POST" action="{{route('transaksi_konven.store')}}" accept-charset="UTF-8">
           {{csrf_field()}}
          <div class="box">  
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Belanja</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{csrf_field()}}
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <th>Jumlah</th>
                  <th>Subtotal</th>
                  <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody id="daftar"></tbody>
              </table>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->


        <div class="col-md-5">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pembayaran</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                {{csrf_field()}}
                <div class="row">
                  
                  <div class="col-md-6">
                  <label for="id">Total Belanja</label>
                  <input class="form-control" type="text" id="total" name="total" readonly>
                  @if ($errors->has('total'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('total') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-6">
                      <label for="jumlah">Id Pembeli</label>
                      <input class="form-control" id="id_pembeli" name="id_pembeli" placeholder="Masukan Id Pembeli" type="text" readonly="true">
                  </div>
                  
                </div>
                <div class="row">
                  
                  <div class="col-md-6">
                  <label for="bayar">Jumlah Bayar</label>
                  <input class="form-control" type="text" id="bayar" name="bayar" value="0">
                    @if ($errors->has('bayar'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('bayar') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-6">
                      <label for="jumlah">Nama Pembeli</label>
                      <input class="form-control" id="nama_pembeli" name="nama_pembeli" placeholder="Nama Pembeli">
                      <div id="listPembeli"></div>
                      {{ csrf_field() }}
                  </div>
                  
                </div>
                <div class="row">
                  
                  <div class="col-md-6">
                  <label for="kembalian">Kembalian</label>
                  <input class="form-control" type="text" id="kembali" name="kembali" value="0" readonly>
                    @if ($errors->has('kembalian'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('kembalian') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-6">
                      <label for="jumlah">Saldo Pembeli</label>
                      <input class="form-control" id="saldo_pembeli" name="saldo_pembeli" placeholder="Saldo Pembeli" type="text" readonly>
                  </div>
                  
                </div>
                <div class="row">

                  <div class="col-md-6">
                  <label for="diskon">Total Diskon</label>
                  <input class="form-control" type="text" id="diskon" name="diskon" value="0" readonly>
                    @if ($errors->has('diskon'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('diskon') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="col-md-6">
                    <label for="jenis_pembayaran">Jenis Pembayaran</label>
                    <select class="form-control nav" name="jenis_pembayaran" >
                        <option value="">Pilih Jenis Pembayaran</option>
                        @foreach($jenis_pembayaran as $value => $key)
                        <option value="{{$key->id}}" {{collect(old('jenis_pembayaran'))->contains($key->id) ? 'selected':''}}>{{$key->jenis_pembayaran}}</option>
                        @endforeach                           
                    </select> 
                 
                  </div>
                  
                </div><br>

                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Bayar <i class="fa fa-check" ></i></button>
                   
                </div>
            </div>
             
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
          </form> 
          <!-- /.form store -->
        </div>
        <!-- /.col -->
        

      </div>
      <!-- /.row -->
    <br><br><br> 

    <!-- Modal -->

    </section>    
    
@endsection
