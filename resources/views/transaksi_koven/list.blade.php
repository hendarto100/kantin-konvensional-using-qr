@section('js')

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Transaksi
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Dartar Transaksi</a></li>
        <li class="active">Gamapay</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

        <form action="{{ url('transaksi_konven/export') }}" method="get" class="form-inline" enctype="multipart/form-data">
            <div class="exportexcel">           
              
                <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                        <input placeholder="Masukan tanggal awal" class="form-control datepicker" name="tgl_awal">
                    </div>
                </div>
               <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                        <input placeholder="Masukan tanggal akhir" class="form-control datepicker" name="tgl_akhir">
                    </div>
              </div>

              <button type="submit" class="btn btn-success">Export Excel</button>
       
            </div>
        </form><br>

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Transaksi</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">                  
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Pembeli</th>
                  <th>Jenis Pembayaran</th>
                  <th>Status</th>
                  <th>Total Diskon</th>
                  <th>Total</th>
                  <th>Tanggal</th>
                  <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $value => $transaksi)
                <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$transaksi->nama}}</td>
                  <td>{{$transaksi->jenis_pembayaran}}</td>
                  <td>{{$transaksi->status}}</td>
                  <td>{{$transaksi->diskon}}</td>
                  <td>{{$transaksi->total}}</td>
                  <td>{{\Carbon\Carbon::parse($transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}</td>
                  <td>
                    <a href="{{url('transaksi_konven/detail_transaksi_konven/'.$transaksi->id.'')}}" class="btn btn-primary btn-icon rounded-circle mg-r-5 mg-b-10" title="Lihat Detail">
                        Detail
                      </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
              </div>
              <!-- ./box-body -->
              <div class="box-footer" align="center">
                
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>    
    
@endsection
