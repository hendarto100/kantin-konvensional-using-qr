@section('js')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">

    $('#search').on('keyup',function(){
      $value=$(this).val();
      $.ajax({
        type : 'get',
        url : '{{URL::to('transaksi_konven/search')}}',
        data:{'search':$value},
        success:function(data){
          $('#list').html(data);
        }  
      }); 
    })

</script>
 
<script type="text/javascript">
  $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

<script type="text/javascript">

$(document).ready(function(){

    $('#id_produk').keyup(function(){
                var idfromfield = $('#id_produk').val();
                var url= "{{url('transaksi_konven')}}/"+idfromfield+"/getData"
                $.get(url).done(function(result){
                  $('#harga').val(result.harga)
                  $('#nama').val(result.nama)

                })
              });

                var subTotal = 0;
                $('#jumlah').keyup(function(){
                  var jumlahBarang = $('#jumlah').val();
                  var hargaBarang = $('#harga').val();
                  var subTotal = jumlahBarang*hargaBarang;

                  $('#subtotal').val(subTotal);
                });
                  $('#total').val(total);
});

</script>


<script type="text/javascript">
$(document).ready(function(){
  
        $("#tambah").click(function(){
            var id_produk = $("#id_bproduk").val();
            var nama = $("#nama").val();
            var harga = $("#harga").val();
            var jumlah = $("#jumlah").val();
            var subtotal = $("#subtotal").val();
            
            var markup = "<tr><td><input type='hidden' value='"+id_produk+"' name='id_produk[]'>"+id_produk+"</td><td><input type='hidden' value='" + nama + "' name='nama[]'>"+nama+"</td><td><input type='hidden' value='" + harga + "' name='harga[]'>"+harga+"</td><td><input type='hidden' value='" + jumlah + "' name='jumlah[]'>"+jumlah+"</td><td><input type='hidden' value='" + subtotal + "' name='subtotal[]'>"+subtotal+"</td><td><input type='button' value='Hapus' id='hapus' class='btn btn-danger' name='record'></td></tr>";
            
            $("table tbody").append(markup);
            var totalHarga = $("#total").val();
            if(totalHarga==null){
              totalHarga=0;
            }
              totalHarga=parseInt(totalHarga)+parseInt(subTotal);

            $('#total').val(totalHarga);
            
        });
      
      $(document).on("click",'.btn-danger', function(){
          $(this).closest('tr').remove();
          var subTotal = $("#sub_total").val();
          var totalHarga = $("#total").val();
              totalHarga=parseInt(totalHarga)-parseInt(subTotal);

            $('#total').val(totalHarga);
      });

      $(document).on('keyup','#bayar',function(e){
        
          var bayar = $('#bayar').val();
          var total = $('#total').val();
          var value = parseInt(bayar)-parseInt(total);
          $("#kembali").val(value);
        
      });
}); 

</script>


<!-- <script type="text/javascript">
$(document).ready(function(){

    $('#tambah').click(function(){
      var id = $('#id_produk').val();
      var nama = $('#nama').val();
      var harga = $('#harga').val();
      var jumlah = $('#jumlah').val();
      var subtotal = $('#subtotal').val();

      var markup = "<tr><td><input type='hidden' value='"+id+"' name='id[]'>"+id+"</td><td><input type='hidden' value='" + nama + "' name='nama[]'>"+nama+"</td><td><input type='hidden' value='"+harga+"' name='harga[]'>"+harga+"</td><td><input type='hidden' value='"+jumlah+"' name='jumlah[]'>"+jumlah+"</td><td><input type='hidden' value='"+subtotal+"'name='subtotal[]'>"+subtotal+"</td><td><input type='button' value='Hapus' id='hapus' class='btn btn-danger' name='record'></td></tr>";

      $('#daftar').append(markup);
      var totalHarga = $('#total').val();
      if(totalHarga==null){
        totalHarga=0;
      }
        totalHarga=parseInt(totalHarga)+parseInt(subtotal);

      $('#total').val(totalHarga);
    });

    $(document).on("click",'.btn-danger.',function(){
      $(this).closest('tr').remove();
      var subtotal = $("#subtotal").val();
      var totalHarga = $("#total").val();
        totalHarga=parseInt(totalHarga)-parseInt(subtotal);

        $("#total").val(totalHarga);
    });

    $(document).on('keyup','#bayar',function(e){
      
      var bayar = $("#bayar").val();
      var total $("#total").val();
      var value = parseInt(bayar)-parseInt(total);
      $("#kembali").val(value);
    });
});

</script> -->

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi Baru
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Transaksi</a></li>
        <li class="active">Transaksi Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>      

      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Cari Produk</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{csrf_field()}}
                <!-- baris 1 -->
                <div class="row">
                      <div class="col-md-6">
                          <input class="form-control" id="search" name="search" placeholder="Minimal 3 huruf...." type="text">
                      </div>
                      
                </div>
            <br><br>
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody id="list"></tbody>
            </table>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Input Produk</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{csrf_field()}}
                <!-- baris 1 -->
                <div class="row">

                      <div class="col-md-6">
                        <label for="id">Id Produk</label>
                        <input class="form-control" id="id_produk" name="id_produk" placeholder="Masukan id produk" type="text">
                      </div>
                      <div class="col-md-6">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" id="jumlah" name="jumlah" placeholder="Masukan jumlah beli" type="text">
                      </div>
                </div>
                <!-- baris 2 -->
                <div class="row">
                      <div class="col-md-6">
                        <label for="nama">Nama Produk</label>
                        <input class="form-control" id="nama" name="nama" placeholder="Nama produk" type="text" readonly="true">
                      </div>
                      <div class="col-md-6">
                        <label for="Harga">Harga</label>
                        <input class="form-control" id="harga" name="harga" placeholder="Harga Produk" type="text" readonly="true">
                      </div>
                </div>

                <div class="row">
                      <div class="col-md-12">
                        <label for="subtotal">Subtotal</label>
                        <input class="form-control" id="subtotal" name="subtotal" placeholder="Jumlah subtotal" type="text" readonly="true">                        
                      </div>                    
                </div>
                <br>
                <center><input type="button" name="tambah" id="tambah" value="Tambah produk" class="btn btn-primary"></center>

            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    <div class="row">
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Belanja</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{csrf_field()}}
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                  <th>Subtotal</th>
                  <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->


        <div class="col-md-3">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pembayaran</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {{csrf_field()}}
                <div class="form-group">
                  
                  <label for="total">Total Belanja</label>
                  <input class="form-control" type="text" name="total" placeholder="Total Belanja">
                    @if ($errors->has('total'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('total') }}</strong>
                    </span>
                    @endif
                  
                </div>
                <div class="form-group">
                  
                  <label for="bayar">Jumlah Bayar</label>
                  <input class="form-control" type="text" name="bayar" placeholder="Jumlah Bayar">
                    @if ($errors->has('bayar'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('bayar') }}</strong>
                    </span>
                    @endif
                  
                </div>
                <div class="form-group">
                  
                  <label for="kembalian">Jumlah Kembalian</label>
                  <input class="form-control" type="text" name="kembalian" placeholder="Jumlah Kembalian">
                    @if ($errors->has('kembalian'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $errors->first('kembalian') }}</strong>
                    </span>
                    @endif
                  
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="#">Simpan </a>
                    <a class="btn btn-warning" href="#">Generate QR-Code </a>
                </div>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    <br><br><br> 

    </section>    
    
@endsection
