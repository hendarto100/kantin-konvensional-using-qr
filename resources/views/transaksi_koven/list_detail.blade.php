@section('js')

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Detail Transaksi
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Dartar Detail Transaksi</a></li>
        <li class="active">Gamapay</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Detail Transaksi</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">                  
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Barang</th>
                  <th>Id Transaksi</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <th>jumlah</th>
                  <th>Total</th>
                  <th>Tanggal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $value => $transaksi)
                <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$transaksi->nama}}</td>
                  <td>{{$transaksi->transaksi_konven_id}}</td>
                  <td>{{$transaksi->harga}}</td>
                  <td>{{$transaksi->diskon}}</td>
                  <td>{{$transaksi->jumlah}}</td>
                  <td>{{$transaksi->total}}</td>
                  <td>{{\Carbon\Carbon::parse($transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
              </div>
              <!-- ./box-body -->
              <div class="box-footer" align="center">
                
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>    
    
@endsection
