@extends('master')

@section('css')

@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dshboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/jenis-pembayaran')}}">Jenis Pembayaran</a> /
  <a href="{{url('#')}}">Ubah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Jenis Pembayaran</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          
            {!! Form::open(array('url'=>'master/jenis-pembayaran/edit/'.$data->id.'', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}
            @if(count($errors)>0)
              <div class="alert alert-warning">
                @foreach($errors->all() as $error)
                  <li>
                    {{$error}}
                  </li>
                @endforeach
              </div>
            @endif
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Jenis Pembayaran <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control" placeholder="jenis pembayaran" name="nama" value="{{old('nama') ? old('nama') : $data->jenis_pembayaran}}" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea class="form-control" placeholder="tulis deskripsi" name="deskripsi" value="" type="text" class="form-control col-md-7 col-xs-12">{{old('deskripsi') ? old('deskripsi') : $data->deskripsi}}</textarea>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/jenis-pembayaran')}}'">Batal</a>
                <button id="send" type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>

<!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
