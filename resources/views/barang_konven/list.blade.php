@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="{{url('#')}}">Data Master</a> /
  <a href="#">Produk</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')

        <div><br>  
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>    
        
            <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Produk<small>dari Data Master</small></h2>
                        <div class="clearfix">
                        </div>
                      </div>
                      <div class="x_content">
                        <a href="{{url('master/barang_konven/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Tambah Produk</a>
                        <table id="datatable1" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <th class="col-md-1" >No</th>
                              <!-- <th>Id</th> -->
                              <th>Gambar</th>
                              <th>Penjual</th>
                              <th>Nama Produk</th>
                              <th>Kategori</th>
                              <th>Deskripsi</th>
                              <th class="col-md-2">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($list as $value => $brg)
                            <tr class="item-{{$brg->id}}">
                              <td align="center">{{$value +1}}</td>
                              <td> <img style="width:30px" class="img-responsive img-circle avatar-view" src="{{asset('images/'.$brg->foto.'')}}" alt="Avatar" title="Change Photos"></td>
                              <!-- <td align="center">{{$brg->id}}</td> -->
                              <td>{{$brg->penjual_konven_id}}</td>
                              <td>{{$brg->nama}}</td>
                              <td>{{$brg->kategori}}</td>
                              <td>{{$brg->deskripsi}}</td>
                              <td>
                                <a href="{{url('master/barang_konven/edit/'.$brg->id.'')}}" class="btn btn-warning btn-icon" title="Edit">
                                  <div><i class="fa fa-pencil"></i></div>
                                </a>
                                <button class="delete-modal btn btn-danger" data-id="{{$brg->id}}">
                                  <span class="fa fa-trash"></span>
                                </button>
                                {{-- <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{ url('master/barang_konven/delete/'.$brg->id.'')}}'};"
                                class="btn btn-danger btn-icon" title="Delete">
                                  <div><i class="fa fa-trash"></i></div>
                                </a> --}}
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="deleteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                                <i class="fa fa-4x fa-trash"></i>
                                <h5>Yakin ingin menghapus data?</h5>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                  Batal
                                </button>
                                <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                  Hapus
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('javascript')

    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                aaSorting: false,
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

        });
    </script>
    <script type="text/javascript">
        var id;
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/barang/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

@endsection
