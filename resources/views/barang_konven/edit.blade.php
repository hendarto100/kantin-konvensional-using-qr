@extends('master')

@section('css')
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/barang')}}">Produk</a> /
  <a href="{{url('#')}}">Ubah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Produk</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

            {!! Form::open(array('url'=>'master/barang_konven/edit/'.$data->id.'', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate',  'enctype'=>'multipart/form-data', 'files'=>'true'))!!}
            @if(count($errors)>0)
              <div class="alert alert-warning">
                @foreach($errors->all() as $error)
                  <li>
                    {{$error}}
                  </li>
                @endforeach
              </div>
            @endif
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="img-responsive avatar-view" src="{{asset('images/'.$data->foto.'')}}" alt="Avatar" title="Change Photos">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gambar
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="kategori" class="form-control col-md-7 col-xs-12">
                    <option value="">Choose Categeory</option>
                    @foreach($kategori as $value)
                        <option value="{{$value->id}}" {{collect(old('kategori'))->contains($value->id) ? 'selected':''}} @if($value->id == $data['kategori_id']) selected='selected' @endif>{{$value->kategori}}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Nama Produk <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="nama" value="{{old('nama') ? old('nama') : $data->nama}}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea class="form-control" placeholder="tulis deskripsi" name="deskripsi" value="" type="text" class="form-control col-md-7 col-xs-12">{{old('deskripsi') ? old('deskripsi') : $data->deskripsi}}</textarea>
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Harga <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="harga" value="{{old('harga') ? old('harga') : $data->harga}}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Stok<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="stok" name="stok" value="{{old('stok') ? old('stok') : $data->stok}}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Diskon <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="diskon" value="{{old('diskon') ? old('diskon') : $data->diskon}}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/barang_konven')}}'">Batal</a>
                <button id="send" type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>
  
        <!-- row -->
@endsection

@section('javascript')
  <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
