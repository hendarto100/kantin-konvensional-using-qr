@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="#">Report</a> /
  <a href="{{url('/master/penjual')}}">Seller</a>

@stop

@section('title')
  <h3>Report</h3>
@stop

@section('content')
  <div class="x_panel">
    <div class="x_title">
      <h2 style="margin-top:15px">Seller's Report</h2>
      {!! Form::open(array('url'=>'laporan/penjual/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
        {{-- <div class="form-group pull-right">
          <div class="col-md-12 col-sm-12 col-xs-12 pull-right"> --}}
            <div class="item form-group pull-right">
              <button class="btn btn-primary pull-right" type="submit"> GO! </button>
              <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                <input type="text"  name="until" class="form-control datepicker" value="{{\Carbon\Carbon::parse($requested->until)->addDay('-1')->format('m/d/Y')}}">
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                {{-- <input type="hidden" name="penjual" value="{{$requested->penjual}}"> --}}
                <input type="text"  name="from" class="form-control datepicker" value="{{\Carbon\Carbon::parse($requested->from)->format('m/d/Y')}}">
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                <select class="form-control select2" name="penjual"  class="form-control col-md-7 col-xs-12">
                    {{-- <option value="{{$penjual->id}}">{{$penjual->name}}</option>
                    <optgroup label="Find Seller"> --}}
                      {{-- <option value="">Find Seller</option> --}}
                    @foreach($penjualAll as $value => $key)
                        <option value="{{$key->id}}" @if($key->id == $penjual->id) selected @endif>{{$key->name}}</option>
                    @endforeach
                </select>
              </div>
            </div>
          {{-- </div>
        </div> --}}
      {!!Form::close()!!}
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

          <a href="{{url('master/pembeli/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Export Excel</a>
          <table id="datatable1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Barang</th>
                <th>Nama Toko</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th class="col-md-2">Total</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $pembeli)
              <tr class="item-{{$pembeli->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$pembeli->nama}}</td>
                  <td>{{$pembeli->nama_toko}}</td>
                  <td>{{$pembeli->harga}}</td>
                  <td>{{$pembeli->jumlah}}</td>
                  <td>{{$pembeli->total}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
  </div>
</div>
@if (count($errors)>0)
  <div id="myError" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                  <h4 style="color:red">Ooops...</h4>
                  @foreach($errors->all() as $error)
                    <a>{{$error}}<br></a>
                  @endforeach
              </div>
          </div>
      </div>
  </div>
@endif
@if (session('error'))
  <div id="myError" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                  <h4 style="color:red">Ooops...</h4>
                    <a>{{ session('error') }}</a>
              </div>
          </div>
      </div>
  </div>
@endif
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="http//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script>
          $(function(){
              'use strict';

              $('#datatable1').DataTable({
  //                scrollX: true,
                  responsive: false,
                  language: {
                      searchPlaceholder: 'Search...',
                      sSearch: '',
                      lengthMenu: '_MENU_ items/page',
                  }
              });

              // Select2
              $('.select2').select2();

          });
      </script>
      <script type="text/javascript">
       $(function(){
        $(".datepicker").datepicker({
          // format: "dd/mm/yyyy",
          endDate: "dd",
          autoclose: true,
          todayHighlight: true,
        });
       });
      </script>
      <script>$("#myModalError").modal("show");</script>
      <script>$("#myError").modal("show");</script>


@endsection
