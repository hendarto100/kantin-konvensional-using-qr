
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Kantin Kejujuran UGM">
    <meta name="author" content="KOMSI">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('img/logo_pln.jpg')}}">


    <title>{{env('SITE_TITLE','Kantin Kejujuran UGM')}}</title>

    <!-- vendor css -->
    <link href="{{asset('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">


    @yield('css')

    <link rel="stylesheet" href="{{asset('css/starlight.css')}}">
</head>

<body>

<!-- ########## START: LEFT PANEL ########## -->
<div class="sl-logo"><a href="{{url('/')}}"><img src="{{asset('img/logo_pln3.png')}}" width="25"> KAJUR UGM</a></div>
<div class="sl-sideleft">
    <label class="sidebar-label">Navigation</label>
    <div class="sl-sideleft-menu">
        <a href="{{url('/')}}" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon fa fa-home tx-22"></i>
                <span class="menu-item-label">Dashboard</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->

        <a href="{{url('penjual')}}" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon fa fa-building tx-20"></i>
                <span class="menu-item-label">Penjual</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <a href="{{url('pembeli')}}" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-body tx-20"></i>
                <span class="menu-item-label">Pembeli</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->

        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon fa fa-copy tx-20"></i>
                <span class="menu-item-label">Report</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div>
            <!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{url('report/waktu')}}" class="nav-link">Barang</a></li>
            <li class="nav-item"><a href="{{url('report/penjual')}}" class="nav-link">Penjual</a></li>
            <li class="nav-item"><a href="{{url('report/pembeli')}}" class="nav-link">Pembeli</a></li>
        </ul>
        @if(Auth::user()->level=='Admin')
        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-android-person tx-20"></i>
                <span class="menu-item-label">User Management</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div>
            <!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{url('user-management/user')}}" class="nav-link">User</a></li>
        </ul>
        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
                <i class="menu-item-icon fa fa-server tx-20"></i>
                <span class="menu-item-label">Data Master</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div>
            <!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{url('master/barang')}}" class="nav-link">Barang</a></li>
            <li class="nav-item"><a href="{{url('master/barang_konven')}}" class="nav-link">Barang Konvensional</a></li>
            <li class="nav-item"><a href="{{url('master/jenis-pembayaran')}}" class="nav-link">Jenis Pembayaran</a></li>
            <li class="nav-item"><a href="{{url('master/kategori')}}" class="nav-link">Kategori Barang</a></li>
            <li class="nav-item"><a href="{{url('master/pembeli')}}" class="nav-link">Pembeli</a></li>
            <li class="nav-item"><a href="{{url('master/penjual')}}" class="nav-link">Penjual</a></li>
            <li class="nav-item"><a href="{{url('master/status')}}" class="nav-link">Status</a></li>
            <li class="nav-item"><a href="{{url('master/status-harian')}}" class="nav-link">Status Stok Harian</a></li>
        </ul>
        @endif
    </div><!-- sl-sideleft-menu -->

    <br>
</div><!-- sl-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="sl-header">
    <div class="sl-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
    </div><!-- sl-header-left -->
    <div class="sl-header-right">
        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name">{{Auth::user()->name}}</span>
                    <img src="{{asset('img/avatar.png')}}" class="wd-32 rounded-circle" alt="">
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-200">
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href="{{url('profile')}}"><i class="icon ion-ios-person-outline"></i> Profile</a></li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- sl-header-right -->
</div><!-- sl-header -->
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: MAIN PANEL ########## -->
<div class="sl-mainpanel">
    @yield('breadcrumbs')

    <div class="sl-pagebody">
        @yield('title')

        @yield('content')


    </div><!-- sl-pagebody -->
    <footer class="sl-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2018. Kantin Kejujuran UGM. Love you mom :*.</div>
            {{--<div>Made by ThemePixels.</div>--}}
        </div>
    </footer>
</div><!-- sl-mainpanel -->

<!-- ########## END: MAIN PANEL ########## -->

<script src="{{asset('lib/jquery/jquery.js')}}"></script>
<script src="{{asset('lib/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('lib/popper.js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>

<script src="{{asset('js/starlight.js')}}"></script>

<script src="{{asset('js/sweetalert2.all.js')}}"></script>

<script src="{{asset('js/konami.js')}}"></script>
<script src="{{asset('js/code.js')}}"></script>

@yield('javascript')

</body>
</html>
