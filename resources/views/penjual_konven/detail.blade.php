@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">User</a> /
  <a href="{{url('/master/penjual')}}">Penjual</a> /
  <a href="{{url('#')}}">Detail</a>
@stop

@section('title')
  <h3>User</h3>
@stop

@section('content')
  <div class="x_panel">
    <div class="x_title">
      <h2>Detail</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <div class="col-md-2 col-sm-2 col-xs-12 profile_left" style="margin-bottom:10px">
        <div class="profile_img">
          <div id="crop-avatar">
            <img class="img-responsive img-circle avatar-view" src="{{asset('images/'.$data->user['foto'].'')}}" alt="Avatar" title="Change the avatar">
            <div class="text-center" style="margin-top:10px">
              <a  href="{{url('master/penjual_konven/edit/'.$data->id.'/avatar')}}" class="" title="Edit">
                <div>Ganti Foto</i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 profile_left">
        <label>UMUM</label>
        <table class="table display responsive nowrap">
          <tbody>
            <tr>
              <td class="col-md-2" style="text-align:right"> Email:</td>
              <td>{{$data->user['email']}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Username:</td>
              <td>{{$data->user['name']}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Phone Number:</td>
              <td>{{$data->user['no_telepon']}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Born Day:</td>
              <td>{{\Carbon\Carbon::parse($data->user['tanggal_lahir'])->format('j F Y')}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Saldo:</td>
              <td>Rp {{number_format($data->user['saldo'],0,".",".")}},-</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Bank:</td>
              <td>{{$data->bank['nama']}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Rekening Number:</td>
              <td>{{$data->no_rekening}}</td>
            </tr>
            <tr>
              <td></td>
              <td>
                {{-- <button class="btn btn-success" onclick="location.href='{{url('master/penjual')}}'"> Back</button> --}}
                <a style="width:70px" href="{{url('master/penjual_konven/edit/'.$data->id.'')}}" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Edit">
                  <div><i class="fa fa-pencil"> Edit</i></div>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        <label>PASSWORD</label>
        <table class="table display responsive nowrap">
          <tbody>
            <tr>
              <td class="col-md-2" style="text-align:right"> Password:</td>
              <td>********</td>
            </tr>
            <tr>
              <td></td>
              <td>
                {{-- <button class="btn btn-success" onclick="location.href='{{url('master/penjual')}}'"> Back</button> --}}
                <a style="width:70px"  href="{{url('master/penjual_konven/edit/'.$data->id.'/password')}}" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Edit">
                  <div><i class="fa fa-pencil"> Change</i></div>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
@endsection

@section('javascript')
      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
@endsection
