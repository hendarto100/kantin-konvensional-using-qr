@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="#">User</a> /
  <a href="{{url('#')}}">Seller</a>
@stop

@section('title')
  <h3>User</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        @if (Request::url() == url('master/penjual/aktif'))
          <div class="x_title">
            <h2>Active Seller<small>from Master Data</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual/')}}">All Seller</a>
                </li>
                <li><a href="{{url('master/penjual/aktif')}}">Aktive Seller</a>
                </li>
                <li><a href="{{url('master/penjual/non-aktif')}}">Non Active Seller</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @elseif (Request::url() == url('master/penjual/non-aktif'))
          <div class="x_title">
            <h2>Non Active Seller<small>from Master Data</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual/')}}">All Seller</a>
                </li>
                <li><a href="{{url('master/penjual/aktif')}}">Aktive Seller</a>
                </li>
                <li><a href="{{url('master/penjual/non-aktif')}}">Non Active Seller</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @else
          <div class="x_title">
            <h2>All Seller<small>from Master Data</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual_konven/')}}">All Seller</a>
                </li>
                <li><a href="{{url('master/penjual_konven/aktif')}}">Aktive Seller</a>
                </li>
                <li><a href="{{url('master/penjual_konven/non-aktif')}}">Non Active Seller</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @endif
        <div class="x_content">
          <a href="{{url('master/penjual_konven/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Add Seller</a>
          <table id="datatable1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>No Telepon</th>
                <th>Status</th>
                <th class="col-md-3">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $penjual)
              <tr class="item-{{$penjual->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$penjual->id}}</td>
                  <td><a href="#" title="View {{$penjual->name}}"> {{$penjual->name}} </a></td>
                  <td>{{$penjual->email}}</td>
                  <td>{{$penjual->no_telepon}}</td>
                  <td  class="col-md-1 text-center">
                      @if($penjual->status_id==1)
                      <a style="width:70px" href="javascript:if(confirm('Are you sure change status to Inactive?')){window.location.href='{{ url('master/penjual_konven/status/'.$penjual->user_id.'')}}'};" onclick="" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Aktive">
                          <div><i class="fa fa-toggle-on"></i> Aktive</div>
                      </a>
                      @else
                        <a style="width:70px" href="javascript:if(confirm('Are you sure change status to Active?')){window.location.href='{{ url('master/penjual_konven/status/'.$penjual->user_id.'')}}'};" onclick="" class="btn btn-default btn-xs rounded-circle mg-r-5 mg-b-10" title="Inaktive">
                          <div><i class="fa fa-toggle-off"></i> Inactive</div>
                      </a>
                      @endif
                  </td>
                  <td>
                    {{--<div class="col-md-6">--}}
                      <a href="{{url('master/penjual_konven/barang-jual/'.$penjual->id.'')}}" class="btn btn-info btn-xs rounded-circle mg-r-5 mg-b-10" title="Product List">
                          <div><i class="fa fa-shopping-bag"></i> Product</div>
                      </a>
                      {{-- <a href="{{url('master/penjual_penjual/edit/'.$penjual->id.'')}}" class="btn btn-warning btn-xs rounded-circle mg-r-5 mg-b-10" title="Edit">
                        <div><i class="fa fa-pencil"></i></div>
                      </a> --}}
                      <a href="{{url('master/penjual_konven/'.$penjual->id.'')}}" class="btn btn-success btn-xs rounded-circle mg-r-5 mg-b-10" title="Detail">
                        <div><i class="fa fa-eye"></i> Detail</div>
                      </a>
                      <button class="delete-modal btn btn-danger btn-xs" data-id="{{$penjual->id}}" title="Delete">
                        <span class="fa fa-trash"> Delete</span>
                      </button>
                      {{-- <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{ url('master/penjual/delete/'.$penjual->id.'')}}'};" onclick="" class="btn btn-danger btn-xs rounded-circle mg-r-5 mg-b-10" title="Delete">
                          <div><i class="fa fa-trash"></i></div>
                      </a> --}}
                      {{--</div>--}}
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div id="deleteModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body text-center">
                  <i class="fa fa-4x fa-trash"></i>
                  <h5>Sure to delete data?</h5>
                  <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Cancel
                  </button>
                  <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                    Delete
                  </button>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">
        var id;
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/penjual/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

@endsection
