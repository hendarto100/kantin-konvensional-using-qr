@extends('master')

@section('css')
    <!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection

@section('title')
<div class="sl-page-title">
    <h5>DASHBOARD</h5>
</div><!-- sl-page-title -->
@stop

@section('content')
<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-users"></i></div>
      <div class="count">{{ $produk }}</div>
      <h3>Produk</h3>
      {{-- <p>Lorem ipsum psdea itgum rixt.</p> --}}
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-users"></i></div>
      <div class="count">{{ $pembeli }}</div>
      <h3>Pembeli</h3>
      {{-- <p>Lorem ipsum psdea itgum rixt.</p> --}}
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-shopping-cart"></i></div>
      <div class="count">{{ $transaksi }}</div>
      <h3>Transaksi</h3>
      {{-- <p>Lorem ipsum psdea itgum rixt.</p> --}}
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-ticket"></i></div>
      <div class="count">{{ $hari_ini }}</div>
      <h3>Transaksi Hari Ini</h3>
      {{-- <p>Lorem ipsum psdea itgum rixt.</p> --}}
    </div>
  </div>
</div>

<div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Produk Terlaris<small>Hari Ini</small></h2>
            <div class="clearfix">
            </div>
          </div>
          <div class="x_content">
            <table id="datatable2" class="table table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($list as $value => $data)
                <tr>
                  <td class="col-md-1" align="center">{{$value+1}}</td>
                  <td>{{$data->nama}}</td>
                  <td class="col-md-1">
                    <a href="{{url('transaksi/pembeli/'.$data->id.'')}}" class="btn btn-success btn-xs rounded-circle mg-r-5 mg-b-10" title="Detail">
                      <div><i class="fa fa-eye"></i> Detail</div>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>

  <section class="content" id="chart1">
      {!! $chart1 !!}
  </section>


@endsection

@section('javascript')
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('assets/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{asset('assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('assets/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('assets/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('assets/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    {{-- <script src="{{asset('assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/vendors/flot.curvedlines/curvedLines.js')}}"></script> --}}
    <!-- DateJS -->
    <script src="{{asset('assets/vendors/DateJS/build/date.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('assets/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
                // columnDefs: [
                //   {
                //       targets: -1,
                //       className: 'dt-body-right'
                //   }
                // ]
            });
            $('#datatable2').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

        });
    </script>
@endsection
