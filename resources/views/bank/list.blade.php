@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="{{url('#')}}">Master Data</a> /
  <a href="#">Bank</a>
@stop

@section('title')
  <h3>Master Data</h3>
@stop

@section('content')
            <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Bank<small>Dari Master Data</small></h2>
                        <div class="clearfix">
                        </div>
                      </div>
                      <div class="x_content">
                        <a href="{{url('master/bank/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Tambah Bank</a>
                        <table id="datatable1" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>No</th>
                              <!-- <th>Id</th> -->
                              <th>Bank</th>
                              <th>Deskripsi</th>
                              <th class="col-md-2" >Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($list as $value => $bank)
                            <tr class="item-{{$bank->id}}">
                              <td class="col-md-1" align="center">{{$value +1}}</td>
                              <!-- <td align="center">{{$bank->id}}</td> -->
                              <td class="col-md-3">{{$bank->nama}}</td>
                              <td>{{$bank->deskripsi}}</td>
                              <td class="col-md-2">
                                <a href="{{url('master/bank/edit/'.$bank->id.'')}}" class="btn btn-warning btn-icon" title="Edit">
                                  <div><i class="fa fa-pencil"></i></div>
                                </a>
                                <button class="delete-modal btn btn-danger" data-id="{{$bank->id}}">
                                  <span class="fa fa-trash"></span>
                                </button>
                                   {{-- <form id="delete{{ $bank->id }}" action="{{ URL::to('master/bank/delete/'.$bank->id) }}" method="post" class="m-form">
                                       <input type="hidden" name="_method" value="DELETE">
                                       {{ csrf_field() }}
                                   </form> --}}
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="deleteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                                <i class="fa fa-4x fa-trash"></i>
                                <h5>Yakin ingin menghapus data?</h5>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                  Batal
                                </button>
                                <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                  Hapus
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('javascript')

    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
    <script type="text/javascript">
        var id;
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/bank/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>



@endsection
