@extends('master')

@section('css')
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/bank')}}">Bank</a> /
  <a href="{{url('#')}}">Ubah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Bank</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          {{-- <form class="form-horizontal form-label-left" novalidate> --}}
            {!! Form::open(array('url'=>'master/bank/edit/'.$data->id.'', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}
            {{-- <span class="section">Bank Info</span> --}}
            @if(count($errors)>0)
              <div class="alert alert-warning">
                @foreach($errors->all() as $error)
                  <li>
                    {{$error}}
                  </li>
                @endforeach
              </div>
            @endif
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Nama Bank <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="nama bank" name="nama" value="{{old('nama') ? old('nama') : $data->nama}}" type="text" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea class="form-control" placeholder="write description" name="deskripsi" value="" type="text" class="form-control col-md-7 col-xs-12">{{old('deskripsi') ? old('deskripsi') : $data->deskripsi}}</textarea>
                {{-- <textarea class="form-control" placeholder="tulis deskripsi" name="deskripsi" value="{{old('deskripsi') ? old('deskripsi') : $data->deskripsi}}" type="text" class="form-control col-md-7 col-xs-12">{{$data->deskripsi}}</textarea> --}}
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/bank')}}'">Batal</a>
                <button id="send" type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>
{{-- {!! Form::open(array('url'=>'master/barang/edit/'.$data->id.'', 'method'=>'POST'))!!}
    <div class="card pd-10 pd-sm-20">
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Kategori</p>
                  </div>
                  <div class="col-md-8">
                    <select class="form-control" name="kategori">
                        <option value="">Pilih Kategeori</option>
                        @foreach($kategori as $value)
                            <option value="{{$value->id}}" {{collect(old('kategori'))->contains($value->id) ? 'selected':''}} @if($value->id == $data['kategori_id']) selected='selected' @endif>{{$value->kategori}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('kategori')) <p style="color:#ba2525;">{{ $errors->first('kategori') }} </p>@endif
                  </div>
              </div>
          </div>
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Nama Barang</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                      <input type="text" class="form-control" placeholder="nama barang" name="nama" value="{{old('nama') ? old('nama') : $data->nama}}"/>
                      @if ($errors->has('nama')) <p style="color:#ba2525;">{{ $errors->first('nama') }} </p>@endif
                  </div>
              </div>
          </div>
        </div>
        <div class="row cl-md-12" style="float:right ; margin-top: 10px;">
            <div class="col-md-12" style="text-align: center; ">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger" onclick="location.href='{{url('master/barang')}}'">Cancel</button>
            </div>
        </div>
    </div>
{!!Form::close()!!} --}}

        <!-- row -->
@endsection

@section('javascript')
  <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
