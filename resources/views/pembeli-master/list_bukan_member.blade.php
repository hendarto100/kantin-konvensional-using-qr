@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    {{-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"> --}}

@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashborad</a> /
  <a href="#">User</a> /
  <a href="{{url('/master/pembeli_bukan_member')}}">Pembeli Bukan Member</a>
@stop

@section('title')
  <h3>User</h3>
@stop


@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_content">
          
          <table id="datatable1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Id</th>
                <th>Nama</th>
                <th class="col-md-2">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $pembeli)
              <tr class="item-{{$pembeli->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$pembeli->id}}</td>
                  <td name="nama">{{$pembeli->nama}}</td>
                  <td>
                      <a href="{{url('master/pembeli/add/nonmember/'.$pembeli->id.'')}}" class="btn btn-success btn-xs rounded-circle mg-r-5 mg-b-10" title="Jadikan Member">
                        <div><i class="fa fa-eye"></i> Jadikan Member</div>
                      </a>                    
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>


@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
      {{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                // aaSorting: [[3,'asc']],
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
            // $(document).ready(function() {
            //     var t = $('#datatable1').DataTable( {
            //         "columnDefs": [ {
            //             "searchable": true,
            //             "orderable": false,
            //             "targets": 0
            //         } ],
            //         "order": [[ 2, 'asc' ]]
            //     } );
            //
            //     t.on( 'order.dt search.dt', function () {
            //         t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //             cell.innerHTML = i+1;
            //         } );
            //     } ).draw();
            // } );

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">
        var id;
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/pembeli/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

@endsection
