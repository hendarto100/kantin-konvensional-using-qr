@extends('layout')

@section('css')
    <link href="{{asset('lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
        <a class="breadcrumb-item" href="{{url('#')}}">Data Master</a>
        <a class="breadcrumb-item" href="{{url('master/status-harian')}}">Status Stok Harian</a>
        <a class="breadcrumb-item" href="{{url('master/status-harian/add')}}">Tambah Status Stok Harian</a>
    </nav>
@stop

@section('title')
    <div class="sl-page-title">
        <h5>Tambah Status Stok Harian</h5>
    </div><!-- sl-page-title -->
@stop

@section('content')
{!! Form::open(['url'=>'master/status-harian/store'])!!}
    <div class="card pd-10 pd-sm-20">
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Status Stok Harian</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input class="form-control" placeholder="status stok harian" name="nama" value="{{ old('nama') }}" type="text">
                  </div>
              </div>
          </div>
        </div>
        <div class="row cl-md-12" style="float:right ; margin-top: 10px;">
            <div class="col-md-12" style="text-align: center; ">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger" onclick="location.href='{{url('master/status-harian')}}'">Cancel</button>
            </div>
        </div>
    </div>
{{!!Form::close()}}

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
