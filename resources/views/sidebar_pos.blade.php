<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('image/logo-ugm.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> {{Auth::user()->level}}</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="{{url('dashboard_konven')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="">
          <a href="{{url('profil_pos/'.Auth::user()->id.'')}}">
            <i class="fa fa-user"></i> <span>Profil</span>
          </a>
        </li>
        <li class="">
          <a href="{{url('transaksi_konven')}}">
            <i class="fa fa-pencil"></i> <span>Transaksi Baru</span>
          </a>
        </li>
        <li class="">
          <a href="{{url('transaksi_konven/list')}}">
            <i class="fa fa-gg"></i> <span>Daftar Transaksi</span>
          </a>
        </li>
        <li class="">
          <a href="{{url('barang_konven/list_pos')}}">
            <i class="fa fa-suitcase"></i> <span>Data Produk</span>
          </a>
        </li>
        <li class="">
           <a href="{{route('logout')}}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i> <span>Logout</span>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none">
            {{ csrf_field() }}
            </form>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
