@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="{{url('#')}}">Transaksi</a>
@stop

@section('title')
  <h3>Transaction</h3>
@stop

@section('content')
  @if (count($errors)>0)
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                    @foreach($errors->all() as $error)
                      <a>{{$error}}<br></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Pilih Tanggal<small></small></h2>
          <div class="clearfix">
          </div>
        </div>
        {{-- {!! Form::open(array('url'=>'laporan/pembeli/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!} --}}
        {!! Form::open(array('url'=>'transaksi_admin/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
        <div class="x_content">
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Mulai<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="input-group date">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
                  <input type="text" placeholder="MM/DD/YYYY" name="from" class="form-control datepicker" value="{{old('from')}}">
              </div>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Selesai<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="input-group date">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
                  <input type="text" placeholder="MM/DD/YYYY" name="until" class="form-control datepicker" value="{{old('until')}}">
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button class="btn btn-primary" type="submit"> Cari! </button>
          </div>
        </div>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
  @if (session('error'))
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                      <a>{{ session('error') }}</a>
                </div>
            </div>
        </div>
    </div>
  @endif
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
            $('.select2').select2();

        });
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
        // format: "dd/mm/yyyy",
        endDate: "dd",
        autoclose: true,
        todayHighlight: true,
      });
     });
    </script>
    <script>$("#myModalError").modal("show");</script>
    <script>$("#myError").modal("show");</script>


@endsection
